<?php


namespace App\Http\Controllers\Dashboard;


use App\Actions\Dashboard\File\UploadFileAction;
use App\Http\Requests\Dashboard\FileRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Storage;

class FileUploadController extends BaseDashboardController
{
    public function upload(FileRequest $request, UploadFileAction $uploadFileAction): JsonResponse
    {
        return $uploadFileAction->execute($request);
    }

    public function show(){

    }

    public function destroy(Request $request): JsonResponse
    {
        Storage::delete($request->input('old'));
        return response()->json($request->toArray());

    }
}
