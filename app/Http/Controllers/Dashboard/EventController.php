<?php


namespace App\Http\Controllers\Dashboard;


use App\Actions\Dashboard\Event\CreateEventAction;
use App\Actions\Dashboard\Event\UpdateEventAction;
use App\DTO\EventDTO;
use App\Http\Requests\Dashboard\EventRequest;
use App\Models\Event;
use App\Models\EventTheme;
use App\Models\EventType;
use App\Models\Language;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EventController extends BaseDashboardController
{

    public function index(){
        $langs = Language::all();
        $events = Event::all();
        return view('dashboard.pages.event.index',compact('langs','events'));
    }

    public function create(){
        $langs = Language::all();
        $theme = EventTheme::all();
        $type = EventType::all();
        $authors = User::query()->latest()->get();

        return view('dashboard.pages.event.create',compact('langs','theme','type','authors'));
    }

    public function store(EventRequest $request,CreateEventAction $createEventAction): RedirectResponse
    {
        try {
            $event = $createEventAction->execute(EventDTO::fromRequest($request));
            alert()->success($event->getTranslation('title',$request->input('lang'),__('actions.added')));
            return redirect()->route('dashboard.event.index');
        }catch (Exception $exception){
            dd($exception);
            alert()->error(__('dashboard.event.title'),__('actions.not_saved'));
            return redirect()->route('dashboard.event.create')->withInput();
        }
    }

    public function edit(Request $request,Event $event){
        $langs = Language::all();
        $theme = EventTheme::all();
        $type = EventType::all();
        $authors = User::query()->latest()->get();
        $current_lang = $request->get('lang') ?? app()->getLocale();
        return view('dashboard.pages.event.edit',compact('langs','theme','type','authors','event','current_lang'));
    }

    public function update(EventRequest $request,Event $event,UpdateEventAction $updateEventAction): RedirectResponse
    {

        try {
            $event = $updateEventAction->execute(EventDTO::fromRequest($request,EventDTO::UPDATING),$event);
            alert()->success($event->getTranslation('title',$request->input('lang')),__('actions.updated'));
            return redirect()->route('dashboard.event.edit',['event' => $event->id,'lang' => $request->input('lang')]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.event.index'),__('actions.not_updated'));
            return redirect()->route('dashboard.event.edit',['event' => $event->id])->withInput();
        }
    }

    public function destroy(Event $event): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $event->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        abort(404);
    }

}
