<?php


namespace App\Http\Controllers\Dashboard;


use App\Actions\Dashboard\Page\CreatePageAction;
use App\Actions\Dashboard\Page\UpdatePageAction;
use App\DTO\PageDTO;
use App\Http\Requests\Dashboard\PageRequest;
use App\Models\Language;
use App\Models\Page;
use App\Models\PageType;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PageController extends BaseDashboardController
{
    public function index(){
        $langs = Language::all();
        $pages = Page::all();
        $hasRectorPage = is_null(Page::wherePageTypeId(PageType::whereName(PageType::RECTOR)->first()->id)->first());
        $pageTypes = PageType::all();
        return view('dashboard.pages.page.index',compact('langs','pages','hasRectorPage','pageTypes'));
    }

    public function create(Request $request){
        $langs = Language::all();
        $type = PageType::whereName($request->get('type'))->first()->name ?? PageType::SIMPLE;
        $hasRectorPage = is_null(Page::wherePageTypeId(PageType::whereName(PageType::RECTOR)->first()->id)->first());
        if (!$hasRectorPage) redirect()->route('dashboard.page.create');
        return view('dashboard.pages.page.create.'.$type,compact('langs','type'));
    }

    public function store(PageRequest $request, CreatePageAction $createPostAction): RedirectResponse
    {
        $type = PageType::whereName($request->get('type'))->firstOrFail()->name ?? PageType::SIMPLE;
        try {
            $page = $createPostAction->execute(PageDTO::fromRequest($request,PageDTO::CREATING,$type),$type);
            alert()->success($page->title,__('actions.added'));
            return redirect()->route('dashboard.page.edit',['page' => $page->id,'lang' => $request->input('lang'), 'type' => $page->type->name ?? PageType::SIMPLE]);
        }catch (ModelNotFoundException $exception){
            abort(404);
        }
        catch (Exception $exception){
            alert()->error(__('dashboard.page.title'),__('actions.not_saved'));
            return redirect()->route('dashboard.page.create')->withInput();
        }
    }

    public function edit(Request $request, Page $page){
        $langs = Language::all();
        try {
            $current_lang = Language::whereAlias($request->get('lang') ?? app()->getLocale())->firstOrFail();
            return view('dashboard.pages.page.edit.'.$page->type->name,compact('langs','page','current_lang'));
        }catch (ModelNotFoundException $exception){
            abort(404);
        }catch (Exception $exception){
            alert()->error(__('dashboard.page.title'),__('actions.not_saved'));
            return redirect()->route('dashboard.page.index');
        }
    }

    public function update(PageRequest $request,Page $page,UpdatePageAction $updatePageAction): RedirectResponse
    {
        try {
            $page = $updatePageAction->execute(PageDTO::fromRequest($request,PageDTO::UPDATING,$page->type->name ?? PageType::SIMPLE),$page);
            alert()->success($page->title,__('actions.updated'));
            return redirect()->route('dashboard.page.edit',['page' => $page->id, 'lang' => $request->input('lang') ?? app()->getLocale()]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.page.title'),__('actions.not_updated'));
            return redirect()->route('dashboard.page.edit',['page' => $page->id, 'lang' => $request->input('lang') ?? app()->getLocale()])->withInput();
        }
    }

    public function destroy(Page $page): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $page->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        abort(404);
    }
}
