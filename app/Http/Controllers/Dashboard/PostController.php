<?php


namespace App\Http\Controllers\Dashboard;


use App\Actions\Dashboard\Post\CreatePostAction;
use App\Actions\Dashboard\Post\UpdatePostAction;
use App\DTO\PostDTO;
use App\Http\Requests\Dashboard\PostRequest;
use App\Models\Category;
use App\Models\Language;
use App\Models\Post;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\Tags\Tag;
use function Symfony\Component\String\s;

class PostController extends BaseDashboardController
{

    public function index(){
        $langs = Language::all();
        $posts = Post::all();

        return view('dashboard.pages.post.index',compact('langs','posts'));
    }

    public function create(){
        $langs = Language::all();
        $tags = Tag::query()->latest()->get();
        $categories = Category::all();
        $authors = User::query()->latest()->get();
        return view('dashboard.pages.post.create',compact('langs','tags','categories','authors'));
    }

    public function store(PostRequest $request, CreatePostAction $createPostAction): RedirectResponse
    {
        try {
            $post = $createPostAction->execute(PostDTO::fromRequest($request));
            alert()->success($post->title,__('actions.added'));
            return redirect()->route('dashboard.post.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.post.title'),__('actions.not_saved'));
            return redirect()->route('dashboard.post.create')->withInput();
        }
    }

    public function edit(Request $request, Post $post){
        $langs = Language::all();
        $tags = Tag::query()->latest()->get();
        $categories = Category::all();
        $authors = User::query()->latest()->get();
        $current_lang = $request->get('lang') ?? app()->getLocale();
        $selected_tags = array_map(function ($item){
            return $item['id'];
        },$post->tags->toArray());
        return view('dashboard.pages.post.edit',compact('langs','tags','categories','authors','current_lang','post','selected_tags'));
    }

    public function update(PostRequest $request,Post $post, UpdatePostAction $updatePostAction): RedirectResponse
    {
        try {
            $post = $updatePostAction->execute(PostDTO::fromRequest($request,PostDTO::UPDATING),$post);
            alert()->success($post->getTranslation('title',$request->input('lang')),__('actions.updated'));
            return redirect()->route('dashboard.post.edit',['post' => $post->id,'lang' => $request->input('lang')]);
        }catch (Exception $exception){
            alert()->error(__('dashboard.post.index'),__('actions.not_updated'));
            return redirect()->route('dashboard.post.edit',['post' => $post->id])->withInput();
        }
    }

    public function destroy(Post $post): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $post->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        abort(404);
    }

}
