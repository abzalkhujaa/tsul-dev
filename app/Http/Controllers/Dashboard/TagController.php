<?php


namespace App\Http\Controllers\Dashboard;


use App\Actions\Dashboard\Tag\CreateTagAction;
use App\Actions\Dashboard\Tag\UpdateTagAction;
use App\DTO\TagDTO;
use App\Http\Requests\Dashboard\TagRequest;
use App\Models\Language;
use App\Models\Tag;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class TagController extends BaseDashboardController
{

    public function index(){
        $langs = Language::all();
        $tags = Tag::all();

        return view('dashboard.pages.tag.index',compact('langs','tags'));
    }

    public function store(TagRequest $request, CreateTagAction $createTagAction): RedirectResponse
    {
        try {
            $tag = $createTagAction->execute(TagDTO::fromRequest($request));
            alert()->success($tag->name,__('actions.added'));
            return redirect()->route('dashboard.tag.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.tag.title'),__('actions.not_saved'));
            return redirect()->route('dashboard.tag.index')->withInput();
        }
    }

    public function update(TagRequest $request,Tag $tag, UpdateTagAction $updateTagAction): RedirectResponse
    {
        try {
            $tag = $updateTagAction->execute(TagDTO::fromRequest($request),$tag);
            alert()->success($tag->name,__('actions.updated'));
            return redirect()->route('dashboard.tag.index');
        }catch (Exception $exception){
            alert()->error(__('dashboard.tag.index'),__('actions.not_updated'));
            return redirect()->route('dashboard.tag.index')->withInput();
        }
    }

    public function destroy(Tag $tag): JsonResponse
    {
        if (request()->wantsJson()){
            try {
                $tag->delete();
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.deleted'),
                        'text' => __('message.model_deleted')
                    ]
                ]);
            }catch (Exception $exception){
                return response()->json([
                    'status' => 'success',
                    'message' => [
                        'title' => __('message.code_500')
                    ]
                ],500);
            }
        }
        abort(404);
    }

}
