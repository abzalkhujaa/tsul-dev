<?php


namespace App\Http\Controllers\Front;


use App\Models\Category;
use App\Models\Post;
use Spatie\Tags\Tag;

class PostController extends BaseFrontController
{
    public function index(){
        $news = Category::whereDescription(Category::NEWS)->first()->posts->take(9);

        $article = Category::whereDescription(Category::ARTICLE)->first()->posts->take(5);
        $anons = Category::whereDescription(Category::ANNOUNCEMENT)->first()->posts->take(5);
        $tags = Tag::all();
        return view('front.pages.news.index',compact('news','article','anons','tags'));
    }
    public function all(){
        $posts = Post::query()->latest()->get();
        $tags = Tag::all();
        return view('front.pages.news.all',compact('posts','tags'));
    }

    public function tag(Tag $tag, string $alias = null){
        dd($tag);
    }

    public function category(Category $category, string $alias = null){
        dd($category);
    }

    public function show(Post $post){
        $article = Category::whereDescription(Category::ARTICLE)->first()->posts->take(5);
        $anons = Category::whereDescription(Category::ANNOUNCEMENT)->first()->posts->take(5);
        return view('front.pages.news.show',compact('post','article','anons'));
    }
}
