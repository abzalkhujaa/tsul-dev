<?php


namespace App\Http\Controllers\Front;


use App\Models\Category;
use App\Models\Post;

class MainController extends BaseFrontController
{
    public function index(){
        $news = Post::whereCategoryId(Category::whereDescription(Category::NEWS)->first()->id)->latest()->get();
        return view('front.index');
    }
}
