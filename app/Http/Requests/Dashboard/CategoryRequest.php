<?php


namespace App\Http\Requests\Dashboard;


class CategoryRequest extends DashboardRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'nullable|string',
            'lang' => 'required|string|size:2'
        ];
    }
}
