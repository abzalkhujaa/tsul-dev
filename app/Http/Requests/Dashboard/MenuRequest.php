<?php


namespace App\Http\Requests\Dashboard;


class MenuRequest extends DashboardRequest
{

    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'lang' => 'required|string|size:2'
        ];
    }
}
