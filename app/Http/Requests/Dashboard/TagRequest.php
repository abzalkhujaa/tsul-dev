<?php


namespace App\Http\Requests\Dashboard;


use App\Models\Tag;
use App\Rules\UniqueSlugRule;

class TagRequest extends DashboardRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|array',
            'slug' => ['required','string',new UniqueSlugRule(Tag::class)]
        ];
    }
}
