<?php


namespace App\Http\Requests\Dashboard;


use Illuminate\Validation\Rule;

class EventRequest extends DashboardRequest
{
    public function rules() : array
    {
        return [
            'title' => 'required|string',
            'image' => 'required|string',
            'alias' => [Rule::requiredIf(function (){
                return ($this->getMethod() === 'PUT');
            }),'string','max:255'],
            'user_id' => 'required|integer',
            'event_type_id' => 'required|integer',
            'event_theme_id' => 'required|integer',
            'content' => 'required|string',
            'date' => 'required|date',
            'time' => 'required',
            'site' => 'nullable|string',
            'email' => 'nullable|string',
            'address' => 'required|string',
            'place' => 'required|string',
            'organizer' => 'required|string',
            'map' => 'nullable|string',
            'lang' => 'required|string|size:2'
        ];
    }
}
