<?php


namespace App\Http\Requests\Dashboard;


use Illuminate\Validation\Rule;

class PostRequest extends DashboardRequest
{

    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'image' => 'required|string|max:255',
            'alias' => [Rule::requiredIf(function (){
                return ($this->getMethod() === 'PUT');
            }),'string','max:255'],
            'user_id' => 'required|integer',
            'content' => 'required|string',
            'description' => 'required|string|max:100',
            'category_id' => 'required|integer',
            'tags' => 'required_if:category_id,1',
            'lang' => 'required|string|size:2'
        ];
    }

}
