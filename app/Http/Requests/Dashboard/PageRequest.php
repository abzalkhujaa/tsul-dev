<?php


namespace App\Http\Requests\Dashboard;


use App\Models\PageType;
use Illuminate\Validation\Rule;

class PageRequest extends DashboardRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'alias' => [Rule::requiredIf(function (){
                return ($this->getMethod() === 'PUT');
            }),'string','max:255'],
            'image' => 'nullable|string',
            'lang' => 'required|string',
            'video' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR or $this->get('type') === PageType::VIDEO;
            }),
            'full_name' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'place' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'email' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'phone' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'telegram' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'twitter' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'facebook' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            }),
            'biography' => Rule::requiredIf(function (){
               return $this->get('type') === PageType::RECTOR;
            })
        ];
    }
}
