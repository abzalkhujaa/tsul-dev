<?php


namespace App\Http\Requests\Dashboard;


class FileRequest extends DashboardRequest
{
    public function rules(): array
    {
        return [
            'image' => 'image|max:3072',
            'video' => 'file|max:307200',
            'avatar' => 'image|max:3072'
        ];
    }
}
