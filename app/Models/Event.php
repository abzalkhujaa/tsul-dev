<?php

namespace App\Models;

use App\Helpers\Traits\HasAuthor;
use App\Helpers\Traits\HasCreator;
use App\Helpers\Traits\HasUpdater;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string|null $image
 * @property int $user_id
 * @property string|null $content
 * @property string $date
 * @property string $time
 * @property string|null $site
 * @property string|null $email
 * @property string|null $address
 * @property string|null $organizer
 * @property string|null $map
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereOrganizer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User $author
 * @property-read \App\Models\User|null $creator
 * @property-read array $translations
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Query\Builder|Event onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Event withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Event withoutTrashed()
 * @property-read \App\Models\User $user
 * @property int $event_theme_id
 * @property int $event_type_id
 * @property string $place
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEventThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEventTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event wherePlace($value)
 */
class Event extends Model
{
    use HasFactory,HasCreator,HasUpdater,SoftDeletes,HasTranslations,HasAuthor;

    protected $translatable = ['title','content','address','organizer','place'];

    protected $fillable = ['title','alias','content','image','user_id','date','time','site','phone','email','address','organizer','map','place','event_theme_id','event_type_id','created_by','updated_by','deleted_by','created_at'];

    public function visits(): Relation
    {
        return visits($this)->relation();
    }

    public function getViewsAttribute(): int
    {
        return $this->visits()->count();
    }


    public static function fields(): array
    {
        return [
            'title' => [
                'ru' => 'string',
                'en' => 'string',
                'uz' => 'string',
            ],
            'alias' => [
                'ru' => 'string',
                'en' => 'string',
                'uz' => 'string',
            ],
            'content' => [
                'ru' => 'string',
                'en' => 'string',
                'uz' => 'string',
            ],
            'image' => 'string',
            'views' => 'string',
            'shares' => 'string',
            'type' => 'string',
            'place' => 'string',
            'author' => [],
            'date' => 'string',
            'time' => 'time',
            'site' => 'string',
            'email' => 'string',
            'address' => [
                'ru' => 'string',
                'en' => 'string',
                'uz' => 'string',
            ],
            'organizer' => [
                'ru' => 'string',
                'en' => 'string',
                'uz' => 'string',
            ],
            'map' => 'string|url',
            'created_at' => 'datetime'
        ];
    }

}
