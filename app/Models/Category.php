<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property array $title
 * @property string $alias
 * @property array|null $description
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property string|null $icon
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIcon($value)
 */
class Category extends Model
{
    use HasFactory,HasTranslations;

    const NEWS = 'news';
    const ARTICLE = 'article';
    const ANNOUNCEMENT= 'announcement';


    public $timestamps = false;

    protected $fillable = ['title','alias','description','icon'];

    protected $translatable = ['title','description'];

    public $hidden = [
        'description','icon','id'
    ];

    public function posts(): HasMany
    {
        return $this->hasMany(Post::class,'category_id');
    }


}
