<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\EventTheme
 *
 * @property int $id
 * @property array $title
 * @property array $alias
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read int|null $events_count
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTheme whereTitle($value)
 * @mixin \Eloquent
 */
class EventTheme extends Model
{
    use HasFactory,HasTranslations;

    protected $fillable = ['title','alias'];

    protected $translatable = ['title'];

    public $timestamps = false;

    public function events(): HasMany
    {
        return $this->hasMany(Event::class,'event_theme_id');
    }
}
