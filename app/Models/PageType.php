<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\PageType
 *
 * @property int $id
 * @property string $name
 * @property string|null $icon
 * @property-read int|null $pages_count
 * @property mixed|null $inputs
 * @method static \Illuminate\Database\Eloquent\Builder|PageType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageType whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageType whereInputs($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page[] $pages
 * @mixin \Eloquent
 */
class PageType extends Model
{
    use HasFactory;

    const VIDEO = 'video';
    const SIMPLE = 'simple';
    const GALLERY = 'gallery';
    const RECTOR = 'rector';

    protected $fillable = ['name','icon','inputs'];

    public $timestamps = false;

    public $hidden = [
        'inputs','icon','id'
    ];

    public function pages(): HasMany
    {
        return $this->hasMany(Page::class,'page_type_id');
    }
}
