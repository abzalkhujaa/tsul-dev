<?php

namespace App\Models;

use App\Helpers\Traits\HasCreator;
use App\Helpers\Traits\HasUpdater;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string|null $content
 * @property string|null $video
 * @property int $page_type_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page wherePageTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereVideo($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User|null $creator
 * @property-read array $translations
 * @property-read \App\Models\PageType $type
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Query\Builder|Page onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Page withoutTrashed()
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereImage($value)
 */
class Page extends Model
{
    use HasFactory,HasCreator,HasUpdater,SoftDeletes,HasTranslations,HasTranslatableSlug;

    protected $translatable = ['title','alias','content'];

    protected $fillable = ['title','alias','content','page_type_id','created_by','updated_by','deleted_by','image'];

    protected $with = ['type'];

    protected $hidden = ['page_type_id','updated_by','created_by','deleted_by','deleted_at',];


    public function type(): BelongsTo
    {
        return $this->belongsTo(PageType::class,'page_type_id')->withDefault([
            'name' => PageType::SIMPLE
        ]);
    }

    public function contentJson(string $lang): Collection
    {
        if ($this->type->name !== PageType::SIMPLE){
            return collect($this->getTranslation('content',$lang));
        }
        return collect();
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('alias');
    }
}
