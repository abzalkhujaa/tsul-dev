<?php

namespace App\View\Components;

use App\Models\Post;
use Illuminate\View\Component;

class CardVerical extends Component
{
    public Post $post;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.card-verical');
    }
}
