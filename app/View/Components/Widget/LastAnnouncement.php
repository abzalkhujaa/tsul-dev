<?php

namespace App\View\Components\Widget;

use App\Models\Category;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class LastAnnouncement extends Component
{
    public Collection $post;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->post = Category::whereDescription(Category::ANNOUNCEMENT)->first()->posts->take(5);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.widget.last-announcement');
    }
}
