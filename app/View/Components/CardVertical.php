<?php

namespace App\View\Components;

use App\Models\Post;
use Illuminate\View\Component;

class CardVertical extends Component
{

    public Post $post;
    public string $type;
    const SIMPLE = 'simple';
    const BIG = 'big';
    /**
     * Create a new component instance.
     *
     * @param Post $post
     * @param string $type
     */
    public function __construct(Post $post, string $type = 'simple')
    {
        $this->post = $post;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.card-vertical');
    }
}
