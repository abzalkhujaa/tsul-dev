<?php

namespace App\View\Components\Html;

use Illuminate\View\Component;

class Input extends Component
{
//    7.4 ===================
//    public string $type;
//    public string $value = '';
//    public string $name;
//    public string $label;
//    public bool $required;
//    public bool $disabled;
//    public string $pattern = '';
//    7.3 ===================
    public $type;
    public $name;
    public $value = '';
    public $label;
    public $required;
    public $disabled;
    public $pattern = '';


  /**
   * Create a new component instance.
   *
   * @param string $type
   * @param string $name
   * @param string $label
   * @param bool $required
   * @param bool $disabled
   * @param string $pattern
   * @param string $value
   */
    public function __construct(string $type,string $name, string $label,bool $required, bool $disabled, string $pattern = '',string $value = '')
    {
        $this->type = $type;
        $this->name = $name;
        $this->value = $value;
        $this->label = $label;
        $this->required = $required;
        $this->disabled = $disabled;
        $this->pattern = $pattern;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.html..input');
    }
}
