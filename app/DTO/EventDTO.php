<?php


namespace App\DTO;

use App\Helpers\Traits\HasCreatorAndUpdaterDTO;
use App\Http\Requests\Dashboard\EventRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class EventDTO extends DataTransferObject
{
    use HasCreatorAndUpdaterDTO;

    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public string $title;
    public string $alias;
    public string $image;
    public int $user_id;
    public string $content;
    public string $date;
    public string $phone;
    public string $time;
    public string $site;
    public string $email;
    public string $address;
    public string $organizer;
    public string $map;
    public int $event_theme_id;
    public int $event_type_id;
    public string $place;
    public string $lang;




    public static function fromRequest(EventRequest $request,string $type = self::CREATING): self
    {
        $data = [
            'title' => (string)Str::of($request->input('title'))->trim(),
            'user_id' => (int)$request->input('user_id'),
            'event_theme_id' => (int)$request->input('event_theme_id'),
            'event_type_id' => (int)$request->input('event_type_id'),
            'alias' => (!is_null($request->input('alias'))) ? $request->input('alias') : (string)Str::of($request->input('title'))->slug(),
            'image' => (string)Str::of($request->input('image'))->trim(),
            'date' => (string)Str::of($request->input('date'))->trim(),
            'place' => (string)Str::of($request->input('place'))->trim(),
            'phone' => (string)Str::of($request->input('phone'))->trim(),
            'time' => (string)Str::of($request->input('time'))->trim(),
            'site' => (string)Str::of($request->input('site'))->trim(),
            'email' => (string)Str::of($request->input('email'))->trim(),
            'address' => (string)Str::of($request->input('address'))->trim(),
            'organizer' => (string)Str::of($request->input('organizer'))->trim(),
            'map' => (string)Str::of($request->input('map'))->trim(),
            'content' => (string)Str::of($request->input('content'))->trim(),
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;

        return new self($data);
    }


}
