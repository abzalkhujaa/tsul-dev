<?php


namespace App\DTO;

use App\Http\Requests\Dashboard\TagRequest;
use Illuminate\Support\Str;
use Spatie\DataTransferObject\DataTransferObject;


class TagDTO extends DataTransferObject
{

    public array $name;
    public string $slug;

    public static function fromRequest(TagRequest $request): self
    {
        $all_name = collect();

        foreach ($request->input('name') as $lang => $name) {
            $all_name->put($lang,$name);
        }

        return new self([
            'name' => $all_name->toArray(),
            'slug' => (string)Str::of($request->input('slug'))->slug()
        ]);
    }

}
