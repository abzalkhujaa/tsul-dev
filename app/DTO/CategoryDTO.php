<?php


namespace App\DTO;

use App\Http\Requests\Dashboard\CategoryRequest;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class CategoryDTO extends DataTransferObject
{
    public string $title;
    public string $description;
    public string $lang;

    public static function fromRequest(CategoryRequest $request): self
    {
        $data = [
            'title' => (string)Str::of($request->input('title'))->trim(),
            'description' => (string)Str::of($request->input('content'))->trim(),
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
        ];

        return new self($data);
    }


}
