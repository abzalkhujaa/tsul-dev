<?php


namespace App\DTO;

use App\Helpers\Traits\HasCreatorAndUpdaterDTO;
use App\Http\Requests\Dashboard\PostRequest;
use App\Models\Tag;
use Spatie\DataTransferObject\DataTransferObject;

use Str;

class PostDTO extends DataTransferObject
{
    use HasCreatorAndUpdaterDTO;

    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public array $title;
    public string $image;
    public int $user_id;
    public array $content;
    public array $description;
    public int $category_id;
    public string $lang;
    public string $alias;
    public ?array $tags;

    public static function fromRequest(PostRequest $request,string $type = self::CREATING): self
    {
        $data = [
            'title' => [$request->input('lang') => (string)Str::of($request->input('title'))->trim()],
            'content' => [$request->input('lang') => (string)Str::of($request->input('content'))->trim()],
            'description' => [$request->input('lang') => (string)Str::of($request->input('description'))->trim()],
            'alias' => (!is_null($request->input('alias'))) ? $request->input('alias') : (string)Str::of($request->input('title'))->slug(),
            'user_id' => (int)$request->input('user_id'),
            'category_id' => (int)$request->input('category_id'),
            'tags' => $request->input('tags'),
            'image' => (string)Str::of($request->input('image'))->trim(),
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;

        return new self($data);
    }

    private static function getTags($tags, string $lang): array
    {
        $tags =  Tag::find($tags);
        $data = [];
        foreach ($tags as $tag){
//            $data[] = ['id' => $tag->id, 'slug' => $tag->getTranslation('slug',$lang)];
            $data[] = $tag->getTranslation('slug',$lang);
        }
        return $data;
    }
}
