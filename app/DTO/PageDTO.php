<?php


namespace App\DTO;

use App\Helpers\Traits\HasCreatorAndUpdaterDTO;
use App\Http\Requests\Dashboard\PageRequest;
use App\Models\PageType;
use Spatie\DataTransferObject\DataTransferObject;
use Str;

class PageDTO extends DataTransferObject
{
    use HasCreatorAndUpdaterDTO;

    public const CREATING = 'creating';
    public const UPDATING = 'updating';

    public array $title;
    public string $alias;
    public string $image;
    public array $content;
    public string $lang;
    public int $page_type_id;


    public static function fromRequest(PageRequest $request,string $type = self::CREATING,string $pageType = PageType::SIMPLE): self
    {
        if ($pageType === PageType::RECTOR)
            return self::fromRectorRequest($request,$type);
        elseif ($pageType === PageType::VIDEO)
            return self::fromVideoRequest($request,$type);
        elseif ($pageType === PageType::GALLERY)
            return self::fromGalleryRequest($request,$type);
        else
            return self::fromSimpleRequest($request,$type);
    }

    private static function fromSimpleRequest(PageRequest $request,string $type = self::CREATING): self
    {
        $data = [
            'title' => [$request->input('lang') => (string)Str::of($request->input('title'))->trim()],
            'content' => [$request->input('lang') => (string)Str::of($request->input('content'))->trim()],
            'image' => (string)Str::of($request->input('image'))->trim(),
            'page_type_id' => PageType::whereName(PageType::SIMPLE)->first()->id,
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;

        return new self($data);
    }

    private static function fromRectorRequest(PageRequest $request, string $type = self::CREATING): self
    {
        $data = [
            'title' => [$request->input('lang') => (string)Str::of($request->input('title'))->trim()],
            'content' => [
                $request->input('lang') => [
                    'content' => (string)Str::of($request->input('content'))->trim(),
                    'video' => (string)Str::of($request->input('video'))->trim(),
                    'full_name' => (string)Str::of($request->input('full_name'))->trim(),
                    'email' => (string)Str::of($request->input('email'))->trim(),
                    'place' => (string)Str::of($request->input('place'))->trim(),
                    'phone' => (string)Str::of($request->input('phone'))->trim(),
                    'telegram' => (string)Str::of($request->input('telegram'))->trim(),
                    'facebook' => (string)Str::of($request->input('facebook'))->trim(),
                    'twitter' => (string)Str::of($request->input('twitter'))->trim(),
                    'biography' => (string)Str::of($request->input('biography'))->trim()
                ]
            ],
            'image' => (string)Str::of($request->input('image'))->trim(),
            'page_type_id' => PageType::whereName(PageType::RECTOR)->first()->id,
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;

        return new self($data);
    }

    private static function fromVideoRequest(PageRequest $request, string $type = self::CREATING): self
    {
        $data = [
            'title' => [$request->input('lang') => (string)Str::of($request->input('title'))->trim()],
            'content' => [
                $request->input('lang') => [
                    'content' => (string)Str::of($request->input('content'))->trim(),
                    'video' => (string)Str::of($request->input('video'))->trim(),
                ]
            ],
            'image' => (string)Str::of($request->input('image'))->trim(),
            'page_type_id' => PageType::whereName(PageType::VIDEO)->first()->id,
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;
        return new self($data);
    }

    private static function fromGalleryRequest(PageRequest $request, string $type = self::CREATING): self
    {
        $data = [
            'title' => [$request->input('lang') => (string)Str::of($request->input('title'))->trim()],
            'content' => [],
            'image' => (string)Str::of($request->input('image'))->trim(),
            'page_type_id' => PageType::whereName(PageType::GALLERY)->first()->id,
            'lang' => (string)Str::of($request->input('lang'))->trim()->lower(),
            'updated_by' => auth()->user()->id ?? 1
        ];
        if ($type === self::CREATING)
            $data['created_by'] = auth()->user()->id ?? 1;
        return new self($data);
    }



}
