<?php


namespace App\Helpers\Traits;


trait HasCreatorAndUpdaterDTO
{
    public ?int $created_by;
    public int $updated_by;


}
