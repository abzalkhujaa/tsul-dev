<?php


namespace App\Helpers\Traits;

use Str;

trait HasAlias
{

    protected static function createAlias(array $title,$model) : string{
        $alias = '';
        foreach ($title as $one){
            if (is_null($one)) continue;
            $alias = Str::slug($one,'-');
            break;
        }
        return self::checkAlias($alias,$model);
    }

    protected static function checkAlias(string $alias,$model) : string{
        $data = $model::whereAlias($alias)->count();
        if ($data > 0) {
            $alias.='-'.Str::random(5);
        }
        return ($data > 0) ? self::checkAlias($alias, $model) : $alias;
    }



}
