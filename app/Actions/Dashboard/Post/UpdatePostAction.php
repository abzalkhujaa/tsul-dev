<?php


namespace App\Actions\Dashboard\Post;


use App\DTO\PostDTO;
use App\Models\Post;
use App\Models\Tag;


class UpdatePostAction
{


    public function execute(PostDTO $data, Post $post): Post
    {
        $post->setTranslation('title',$data->lang,$data->title[$data->lang]);
        $post->setTranslation('content',$data->lang,$data->content[$data->lang]);
        $post->setTranslation('description',$data->lang,$data->description[$data->lang]);
        $post->alias = $data->alias;
        $post->image = $data->image;
        $post->user_id = $data->user_id;
        $post->category_id = $data->category_id;
        $post->created_by = $post->updated_by;
        $post->updated_by = $data->updated_by;
        $post->save();
        $post->syncTags(Tag::find($data->tags));

        return $post;
    }
}
