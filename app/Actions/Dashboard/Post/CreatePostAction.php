<?php


namespace App\Actions\Dashboard\Post;


use App\DTO\PostDTO;
use App\Models\Post;
use App\Models\Tag;

class CreatePostAction
{
    public function execute(PostDTO $data): Post
    {
        $post = Post::create($data->toArray());
        $post->setTranslations('title',$data->title);
        $post->setTranslations('content',$data->content);
        $post->setTranslations('description',$data->description);
        $post->alias = $data->alias;
        $post->save();
        if (!empty($data->tags))
            $post->attachTags(Tag::findMany($data->tags));
        return $post;
    }
}
