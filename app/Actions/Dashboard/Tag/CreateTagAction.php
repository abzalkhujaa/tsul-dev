<?php


namespace App\Actions\Dashboard\Tag;


use App\DTO\TagDTO;
use App\Models\Tag;

class CreateTagAction
{

    public function execute(TagDTO $data): Tag
    {
        $tag = new Tag();
        $tag->setTranslations('name',$data->name);
        $tag->slug = $data->slug;
        $tag->save();

        return $tag;
    }
}
