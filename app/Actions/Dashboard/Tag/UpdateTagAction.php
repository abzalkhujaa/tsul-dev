<?php


namespace App\Actions\Dashboard\Tag;


use App\DTO\TagDTO;
use App\Models\Tag;

class UpdateTagAction
{
    public function execute(TagDTO $data, Tag $tag): Tag
    {
        $tag->replaceTranslations('name',$data->name);
        $tag->slug = $data->slug;
        $tag->save();

        return $tag;
    }
}
