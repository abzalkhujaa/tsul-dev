<?php


namespace App\Actions\Dashboard\Event;


use App\DTO\EventDTO;
use App\Models\Event;

class CreateEventAction
{

    public function execute(EventDTO $data): Event{
        $event = new  Event($data->toArray());
        $event->setTranslation('title', $data->lang,$data->title);
        $event->setTranslation('content', $data->lang,$data->content);
        $event->setTranslation('address', $data->lang,$data->address);
        $event->setTranslation('organizer', $data->lang,$data->organizer);
        $event->setTranslation('place', $data->lang,$data->place);
        $event->save();
        return $event;
    }

}
