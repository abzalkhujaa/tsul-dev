<?php


namespace App\Actions\Dashboard\File;


use App\Http\Requests\Dashboard\FileRequest;
use Illuminate\Http\JsonResponse;

class UploadFileAction
{
    const VIDEO = 'video';
    const IMAGE = 'image';
    const AVATAR = 'avatar';

    private string $file;

    public function execute(FileRequest $request): JsonResponse
    {
        $request = $this->checkTypeFile($request);
        try {
            $file = $request->file($this->file)->store('public/'.$this->file);
            $file = str_replace('public','storage',$file);
            return response()->json(['status' => true,'file' => $file,'old'=>$request->input('old')],200);
        }catch (\Exception $exception){
            return response()->json(['status' => false],500);
        }

    }

    private function checkTypeFile(FileRequest $request): FileRequest
    {
        if ($request->hasFile(self::IMAGE)) $this->file = self::IMAGE;
        if ($request->hasFile(self::VIDEO)) $this->file = self::VIDEO;
        if ($request->hasFile(self::AVATAR)) $this->file = self::AVATAR;
        return $request;
    }
}
