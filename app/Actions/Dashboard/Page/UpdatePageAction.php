<?php


namespace App\Actions\Dashboard\Page;


use App\DTO\PageDTO;
use App\Models\Page;
use Str;

class UpdatePageAction
{
    public function execute(PageDTO $data, Page $page): Page
    {
        $page->setTranslation('title',$data->lang,$data->title[$data->lang]);
        $page->setTranslation('content',$data->lang,$data->content[$data->lang]);
        $page->setTranslation('alias',$data->lang,(string)Str::of($data->title[$data->lang])->slug());
        $page->image = $data->image;
        $page->created_by = $page->updated_by;
        $page->updated_by = $data->updated_by;
        $page->save();
        return $page;
    }
}
