<?php


namespace App\Actions\Dashboard\Page;


use App\DTO\PageDTO;
use App\Models\Page;
use App\Models\PageType;

class CreatePageAction
{
    public function execute(PageDTO $data, string $type): Page
    {
        if ($type === PageType::RECTOR){
            $page = new Page($data->toArray());
            $page->setTranslation('title',$data->lang,$data->title[$data->lang]);
            $page->setTranslation('content',$data->lang,$data->content[$data->lang]);
            $page->image = $data->image;
        }elseif($type === PageType::VIDEO){
            $page = new Page($data->toArray());
            $page->setTranslation('title',$data->lang,$data->title[$data->lang]);
            $page->setTranslation('content',$data->lang,$data->content[$data->lang]);
            $page->image = $data->image;
        }elseif ($type === PageType::GALLERY){
            $page = new Page($data->toArray());
            $page->setTranslation('title',$data->lang,$data->title[$data->lang]);
            $page->setTranslation('content',$data->lang,$data->content[$data->lang]);
            $page->image = $data->image;
        }else{
            $page = new Page($data->toArray());
            $page->setTranslations('title',$data->title);
        }
        $page->save();

        return $page;
    }
}
