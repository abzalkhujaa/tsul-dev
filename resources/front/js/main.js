$('[data-fancybox="images"]').fancybox();

$("#actual_news_slider").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 7000,
    nav: true,
    animateOut: 'fadeOut',
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    },
    navigation: true,
    pagination: true
});

$("#links_slider").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout:7000,
    smartSpeed:1000,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    },
    navigation : true,
    pagination: false
});
