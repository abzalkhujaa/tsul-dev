<?php


return [
    'title' => 'Actions',
    'add' => 'Add',
    'create' => 'Create',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'view' => 'View',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'added' => 'Added',
    'updated' => 'Updated',
    'update' => 'Update',
    "editing" => "Editing",
    "deleted" => "Deleted",
    "success" => "Success",
    "error" => "Error",
    'status' => 'Status',
    "are_you_sure" => "Are you sure ?",
    "dont_revert" => "You won't be able to revert this!",
    "yes_delete_btn" => "Yes, delete it!",
    "no_delete_btn" => "Cancel",
    "you_deleted" => 'This item has been deleted.',
    "dropzone_placeholder" => "Drag and drop a file here or click to select a file <u> Select </u>",
    "dropzone_hint" => "<p><small><sup class='text-danger'>*</sup>When uploading a new file, the old file is deleted</small></p>",
    "dropzone_delete" => "<span class='badge-pill badge-danger mt-3'>Delete</span>",
    "dropzone_upload_error" => "Error in uploading file",
    "dropzone_upload_success" => "File successfully uploaded",

    'create_rector_page' => 'Create rector`s page',
    'create_video_page' => 'Create video page',
    'create_gallery_page' => 'Create gallery page',

];
