<?php

return [
    'tag' => [
        'index' => 'Теги',
        'title' => 'Заголовок тега',
        'slug' => 'URL',
        'create' => 'Создать тег',
        'add' => 'Добавить тег',
        'update' => 'Обновить тег',
        'delete' => 'Удалить тег',
        'edit' => 'Изменить тег',
        'created_at' => 'Создано',
        'select' => 'Choose tags'
    ],
    'post' => [
        'index' => 'Posts',
        'title' => 'Title',
        'slug' => 'URL',
        'description' => 'Description',
        'create' => 'Create post',
        'add' => 'Add post',
        'update' => 'Update post',
        'delete' => 'Delete post',
        'edit' => 'Edit post',
        'created_at' => 'Created',
        'category' => 'Category',


    ],
//    alias
//content
//image
//user_id
//date
//time
//site
//email
//address
//organizer
//map
//place
//event_theme_id
//event_type_id
//created_by
//updated_by
    'event' => [
        'index' => 'Events',
        'title' => 'Title',
        'content' => 'Content',
        'image' => 'Image',
        'date' => 'Date',
        'time' => 'Time',
        'site' => 'Site',
        'email' => 'Email',
        'phone' => 'Phone',
        'address' => 'Address',
        'organizer' => 'Organizer',
        'map' => 'Map',
        'place' => 'Place',
        'alias' => 'URL',
        'create' => 'Create post',
        'add' => 'Add post',
        'update' => 'Update post',
        'delete' => 'Delete post',
        'edit' => 'Edit post',
        'created_at' => 'Created'
    ],
    'page' => [
        'index' => 'Pages',
        'title' => 'Title',
        'slug' => 'URL',
        'create' => [
            'simple' => 'Create page',
            'video' => 'Create video page',
            'rector' => 'Create rector`s page',
            'gallery' => 'Create gallery page',
        ],
        'add' => 'Add page',
        'update' => 'Update page',
        'delete' => 'Delete page',
        'edit' => 'Edit page',
        'created_at' => 'Created',
        'type' => 'Type',
        'rector_greeting' => "Rector's greeting",
        'telegram' => 'Telegram username (@username)',
        'facebook' => 'Facebook account',
        'twitter' => 'Twitter account',
        'place' => 'Place',
        'email' => 'Email',
        'biography' => 'Biography',
        'full_name' => 'Full name',
        'video' => 'Video',
        'image' => 'Image',
        'phone' => 'Phone',
    ],

    'lang' => [
        'index' => 'Languages',
        'title' => 'Language',
        'choose' => 'Choose Language',
    ],

    'category' => [
        'index' => 'Categories',
        'title' => 'Category',
        'select' => 'Choose category',
    ],

    'event_theme' => [
        'index' => 'Themes',
        'title' => 'Theme',
        'select' => 'Choose theme',
    ],

    'event_type' => [
        'index' => 'Types',
        'title' => 'Type',
        'select' => 'Choose type',
    ],

    'author' => [
        'index' => 'Authors',
        'title' => 'Author',
        'select' => 'Select author',
    ]
];
