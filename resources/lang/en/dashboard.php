<?php

return [
    'tag' => [
        'index' => 'Теги',
        'title' => 'Заголовок тега',
        'slug' => 'URL',
        'create' => 'Создать тег',
        'add' => 'Добавить тег',
        'update' => 'Обновить тег',
        'delete' => 'Удалить тег',
        'edit' => 'Изменить тег',
        'created_at' => 'Создано',
        'select' => 'Choose tags'
    ],
    'post' => [
        'index' => 'Posts',
        'title' => 'Title',
        'description' => 'Description',
        'create' => 'Create post',
        'add' => 'Add post',
        'update' => 'Update post',
        'delete' => 'Delete post',
        'edit' => 'Edit post',
        'created_at' => 'Created'
    ],
    'page' => [
        'index' => 'Pages',
        'title' => 'Title',
        'create' => [
            'simple' => 'Create page',
            'video' => 'Create video page',
            'rector' => 'Create rector`s page',
            'gallery' => 'Create gallery page',
        ],
        'add' => 'Add page',
        'update' => 'Update page',
        'delete' => 'Delete page',
        'edit' => 'Edit page',
        'created_at' => 'Created',
        'type' => 'Type',
        'rector_greeting' => "Rector's greeting",
        'telegram' => 'Telegram username (@username)',
        'facebook' => 'Facebook account',
        'twitter' => 'Twitter account',
        'place' => 'Place',
        'email' => 'Email',
        'biography' => 'Biography',
        'full_name' => 'Full name',
        'video' => 'Video',
        'image' => 'Image',
        'phone' => 'Phone',
    ],

    'lang' => [
        'index' => 'Languages',
        'title' => 'Language',
        'choose' => 'Choose Language',
    ],

    'category' => [
        'index' => 'Categories',
        'title' => 'Category',
        'select' => 'Choose category',
    ],

    'author' => [
        'index' => 'Authors',
        'title' => 'Author',
        'select' => 'Select author',
    ]
];
