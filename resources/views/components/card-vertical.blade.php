<div class="news_box">
    <span class="news_img"><img src="{{ asset($post->image) }}" alt="" /></span>
    <div class="news_text_box shadow-sm">
        <a href="{{ route('front.post.show',['post'=>$post->id,'alias' => $post->getTranslation('alias',app()->getLocale())]) }}" class="news_link">
            <span><i>{{ $post->created_at->format('d') }}</i>{{ $post->created_at->format('M') }}</span>
            {{ Str::limit($post->getTranslation('title',app()->getLocale()),59,' ') }}
        </a>
        @if($type == \App\View\Components\CardVertical::SIMPLE)
            <p>
               {{ $post->getTranslation('description',app()->getLocale()) }}
            </p>
            <span class="date">{{ $post->created_at->format('d.m.Y') }}</span>
        @endif
    </div>
</div>
