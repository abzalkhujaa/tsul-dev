<div class="bg_recent_articles">
    <span class="mini_title">Последние статьи</span>
    <ul class="recent_articles">
        @foreach($post as $one)
            <li>
                <span class="round_img"><img src="{{ asset($one->image) }}" alt="" /></span>
                <a href="{{ route('front.news.show',['post' => $one->id]) }}">{{ $one->author->first_name . ' ' . $one->author->last_name }}</a>
                <span class="rec_art_tex">
                                                {{ Str::limit($one->getTranslation('title',app()->getLocale())) }}
                                            </span>
            </li>
        @endforeach
    </ul>
    <a href="{{ route('front.news.index') }}" class="in_all_news_link">Все статьи</a>
</div>
