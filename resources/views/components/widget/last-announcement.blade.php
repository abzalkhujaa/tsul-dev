<div>
    <span class="mini_title">Анонсы</span>
    <ul class="anons_list">
        @foreach($post as $one)
            <li>
                <p><span>{{ $one->created_at->format('d') }}<i>{{ $one->created_at->format('M') }}</i></span></p>
                <a
                    href="{{ route('front.news.show',['post' => $one->id]) }}"
                    title="{{ $one->getTranslation('title',app()->getLocale()) }}">
                    {{ Str::limit($one->getTranslation('title',app()->getLocale()),55) }}
                </a>
            </li>
        @endforeach
    </ul>
    <a href="{{ route('front.news.all') }}" class="in_all_news_link">Все анонсы</a>
</div>
