<div>
    <span class="mini_title mini_title_news_in">Последние новости</span>
    <div class="left_side_b">
        <div class="news_in_main">
            <span class="news_in_main_img"><img src="{{ asset('front/img/news_in_img1.jpg') }}" alt="" /></span>
            <span class="news_type">Образование</span>
            <a href="#" class="news_in_title">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</a>
            <span class="date">28.12.2020</span>
        </div>
        <div class="news_in_main">
            <span class="news_in_main_img"><img src="{{ asset('front/img/news_img2.jpg') }}" alt="" /></span>
            <span class="news_type">Образование</span>
            <a href="#" class="news_in_title">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</a>
            <span class="date">28.12.2020</span>
        </div>
        <div class="news_in_main">
            <span class="news_in_main_img"><img src="{{ asset('front/img/info_law_1.jpg') }}" alt="" /></span>
            <span class="news_type">Образование</span>
            <a href="#" class="news_in_title">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</a>
            <span class="date">28.12.2020</span>
        </div>
        <a href="{{ route('front.news.index') }}" class="in_all_news_link">Все новости</a>
    </div>
</div>
