<div class="news_in_main">
    <span class="news_in_main_img"><img src="{{ asset($post->image) }}" alt="" /></span>
        <div class="d-flex">
            @foreach($post->tags as $tag)
                <span class="news_type mb-1">{{ $tag->getTranslation('name',app()->getLocale()) }}</span>
            @endforeach
        </div>
    <a href="{{ route('front.news.show',['post' => $post->id, 'alias' => $post->getTranslation('alias',app()->getLocale())]) }}"
       class="news_in_title mt-1">
        {{ $post->getTranslation('title',app()->getLocale()) }}
    </a>
    <span class="date">{{ $post->created_at->format('d.m.Y') }}</span>
</div>
