<html lang="{{ app()->getLocale() }}"
      data-textdirection="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - TTPU HR платформа</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    @include('vendor.favicon')
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    {{-- Include core + vendor Styles --}}
    @include('dashboard.layouts.panel.style')

</head>
<body class="vertical-layout vertical-menu-modern blank-page light data-menu pace-done menu-hide"
      data-menu="vertical-menu-modern"
      data-col="2-columns"
      data-layout="light"
      data-framework="laravel"
      data-asset-path="{{ asset('/')}}">

{{-- Include Sidebar --}}

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">

            {{-- Include Startkit Content --}}
            @yield('content')

        </div>
    </div>
</div>

{{-- include default scripts --}}
@include('dashboard.layouts.panel.scripts')

<script type="text/javascript">
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14, height: 14
            });
        }
    })
</script>
</body>
</html>

