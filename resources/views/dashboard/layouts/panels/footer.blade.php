<!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0">
      <span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020
        <a class="text-bold-800 grey darken-2" href="/">
          {{ __('dashboard.title') }},
        </a>
        All rights Reserved
      </span>
      <span class="float-md-right d-none d-md-block">
        Crafted with <i class="feather icon-heart pink"></i> by <a href="http://abzalkhuja.uz">Abzalkhujaa</a>
      </span>
      <button class="btn btn-primary btn-icon scroll-top" type="button">
        <i class="feather icon-arrow-up"></i>
      </button>
    </p>
  </footer>
<!-- END: Footer-->
