<div
  class="main-menu menu-fixed menu-light menu-accordion menu-shadow"
  data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="{{ route('dashboard.index') }}">
          <div class="brand-logo"></div>
          <h2 class="brand-text mb-0">CIC Dashboard</h2>
        </a>
      </li>
      <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
          <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon"
             data-ticon="icon-disc">
          </i>
        </a>
      </li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      {{-- Foreach menu item starts --}}
        <li class="navigation-header">
            <span>Dashboard</span>
        </li>
        <li class="nav-item">
            <a href="{{ route('dashboard.tag.index') }}">
                <i class="fas fa-tag"></i>
                <span class="menu-title">{{ __('dashboard.tag.index') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a>
                <i class="fas fa-file-alt"></i>
                <span class="menu-title">{{ __('dashboard.page.index') }}</span>
            </a>
            <ul class="menu-content">
                <li>
                    <a href="{{ route('dashboard.page.index') }}">
                        <i class="fal fa-dot-circle"></i>
                        <span class="menu-title">{{ __('dashboard.page.index') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('dashboard.page.create') }}">
                        <i class="fal fa-dot-circle"></i>
                        <span class="menu-title">{{ __('dashboard.page.create.simple') }}</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a>
                <i class="fas fa-newspaper"></i>
                <span class="menu-title">{{ __('dashboard.post.index') }}</span>
            </a>
            <ul class="menu-content">
                <li>
                    <a href="{{ route('dashboard.post.index') }}">
                        <i class="fal fa-dot-circle"></i>
                        <span class="menu-title">{{ __('dashboard.post.index') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('dashboard.post.create') }}">
                        <i class="fal fa-dot-circle"></i>
                        <span class="menu-title">{{ __('dashboard.post.create') }}</span>
                    </a>
                </li>
            </ul>
        </li>
      @if(isset($sidebarMenu))
        @foreach($sidebarMenu->menu as $menu)
          @if(isset($menu->navheader))
            <li class="navigation-header">
              <span>{{ __($menu->navheader) }}</span>
            </li>
          @else
            <li class="nav-item {{ (request()->route()->getName() === $menu->url) ? 'active' : '' }}">
              <a @if(empty($menu->url))  @else href="{{ route($menu->url) }}" @endif>
                <i class="{{ $menu->icon }}"></i>
                <span class="menu-title">{{ __($menu->name) }}</span>
                @if (isset($menu->badge))
                  <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                  <span
                    class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                @endif
              </a>
              @if(isset($menu->submenu))
                @include('dashboard.layouts.panels.submenu', ['menu' => $menu->submenu])
              @endif
            </li>
          @endif
        @endforeach
      @endif
      {{-- Foreach menu item ends --}}
    </ul>
  </div>
</div>
<!-- END: Main Menu-->
