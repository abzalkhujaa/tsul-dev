<html lang="@if(session()->has('locale')){{session()->get('locale')}}@else{{ app()->getLocale() }}@endif" data-textdirection="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Abzalkhujaa">

    <title>@yield('title') - {{ __('dashboard.title') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.ico">

    {{-- Include core + vendor Styles --}}
    @include('dashboard.layouts.panels.styles')
{{--    @livewireStyles--}}
  </head>

<body
  class="vertical-layout vertical-menu-modern 2-columns theme navbar-floating footer-static menu-expanded pace-done"
  data-menu="vertical-menu-modern"
  data-col="2-columns"
  data-layout="light"
  data-editor="ClassicEditor"
  data-collaboration="false">
{{-- Include Sidebar --}}
@include('dashboard.layouts.panels.sidebar')

<!-- BEGIN: Content-->
<div class="app-content content">
  <!-- BEGIN: Header-->
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>

  {{-- Include Navbar --}}
  @include('dashboard.layouts.panels.navbar')

  @if(@isset($dashboard))
    <div class="content-area-wrapper">
      <div class="default-sidebar-position">
        <div class="sidebar">
          {{-- Include Sidebar Content --}}
          @yield('content-sidebar')
        </div>
      </div>
      <div class="default-sidebar">
        <div class="content-wrapper">
          <div class="content-body">
            {{-- Include Page Content --}}
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  @else
    <div class="content-wrapper">
      {{-- Include Breadcrumb --}}
      @include('dashboard.layouts.panels.breadcrumb')
      <div class="content-body">
        {{-- Include Page Content --}}
        @yield('content')
      </div>
    </div>
  @endif

</div>
<!-- End: Content-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

{{-- include footer --}}
@include('dashboard.layouts.panels.footer')

{{-- include default scripts --}}
@include('dashboard.layouts.panels.scripts')
@include('sweetalert::alert')
{{--@livewireScripts--}}
</body>

</html>
