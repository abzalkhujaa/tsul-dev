@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.content.edit'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="card-content">
        <form action="{{ route('dashboard.content.store') }}" method="POST" class="row was-validated">
          @csrf
          <input type="hidden" name="type_id" value="{{ $type->id }}">
          <input type="hidden" name="url" value="">
          <input type="hidden" name="background" value="">
          <div class="col-md-6 col-12 mb-2">
            <select data-placeholder="{{ __('dashboard.package.select') }}..." name="package_id" class="select2-icons form-control">
              <optgroup label="{{ __('dashboard.package.title') }}">
                @foreach($package as $item)
                  <option value="{{ $item->id }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>
                @endforeach
              </optgroup>
            </select>
          </div>
          <div class="col-md-6 col-12 mb-2">
            <select data-placeholder="{{ __('dashboard.lang.select') }}..." name="lang" class="select2-icons form-control">
              <optgroup label="{{ __('dashboard.lang.title') }}">
                @foreach($lang as $item)
                  <option value="{{ $item->alias }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>
                @endforeach
              </optgroup>
            </select>
          </div>
          <div class="col-md-12 col-12">
            <x-html.input type="text" name="title" :label="__('dashboard.pages.content.input.title')" :required="true" :disabled="false"/>
          </div>
          {{--          <div class="col-md-6 col-12 mb-3">--}}
          {{--            <x-html.input type="text" name="url" :label="__('dashboard.pages.content.input.title')" :required="false" :disabled="false"/>--}}
          {{--          </div>--}}
          {{--          <div class="col-md-6 col-12 mb-3">--}}
          {{--            <x-html.input type="text" name="background" :label="__('dashboard.pages.content.input.title')" :required="false" :disabled="false"/>--}}
          {{--          </div>--}}
          <div class="col-12 mb-3">
            <textarea name="content" id="editor" style="min-width: 200px;"></textarea>
          </div>
          <div class="col-12 mb-3">
            <button type="submit" class="btn btn-block btn-primary">
              {{ __('dashboard.action.save') }}
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->

  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
  {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script>
    ClassicEditor
      .create( document.querySelector( '#editor' ), {

        toolbar: {
          items: [
            'heading',
            'fontFamily',
            'fontSize',
            'fontColor',
            'fontBackgroundColor',
            '|',
            'bold',
            'italic',
            'underline',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            'alignment',
            'indent',
            'outdent',
            '|',
            'imageUpload',
            'imageInsert',
            'htmlEmbed',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            '|',
            'pageBreak',
            'horizontalLine',
            'MathType',
            'ChemType',
            'restrictedEditingException',
            'specialCharacters',
            '|',
            'undo',
            'redo'
          ]
        },
        language: 'ru',
        image: {
          styles: [
            'alignLeft', 'alignCenter', 'alignRight'
          ],
          resizeOptions: [
            {
              name: 'imageResize:original',
              label: 'Original',
              value: null
            },
            {
              name: 'imageResize:50',
              label: '50%',
              value: '50'
            },
            {
              name: 'imageResize:75',
              label: '75%',
              value: '75'
            }
          ],
          toolbar: [
            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
            '|',
            'imageResize',
            '|',
            'imageTextAlternative'
          ]
        },
        table: {
          contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells',
            'tableCellProperties',
            'tableProperties'
          ]
        },
        licenseKey: '',
      } )
      .then( editor => {
        window.editor = editor;
      } )
      .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
        console.error( error );
      } );
  </script>
@endsection


