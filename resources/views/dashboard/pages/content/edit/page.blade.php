@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.content.edit'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
  <form action="{{ route('dashboard.content.update',['content' => $content->id]) }}" method="POST" class="row was-validated">
    @csrf
    @method('PUT')
    <input type="hidden" name="lang" value="{{ $lang }}">
    <input type="hidden" name="url" value="">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12 mb-2">
                <x-html.input type="text" :value="$content->getTranslation('title',$lang)" name="title" :label="__('dashboard.pages.content.input.title')" :required="true" :disabled="false"/>
              </div>
              <div class="col-12 mb-2">
                <textarea name="content" id="editor" style="min-width: 200px;">
                  {!! $content->getTranslation('content',$lang) !!}
                </textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12">
                <div class="card text-center border-primary">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="row align-items-center">
                        <div class="col"><i class="fal {{ $content->type->icon }} fa-5x text-primary"></i></div>
                        <div class="col">
                          <h2 class="card-title mt-1 text-primary">{{ $content->type->title }}</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 mb-2">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#{{ $content->alias }}">
                  <i class="feather icon-eye mr-1"></i>{{ __('dashboard.pages.content.preview') }}
                </button>
                <div class="modal fade text-left" id="{{ $content->alias }}" tabindex="-1" role="dialog"
                     aria-labelledby="content_modal_label" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content h-100">
                      <div class="modal-header">
                        <h4 class="modal-title text-center" id="content_modal_label">{{ $content->title }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <h1 style="font-size: 3rem" class="mb-3 text-center">{{ $content->getTranslation("title",$lang) }}</h1>
                        <div class="container">
                          <img src="{{ asset('storage/'.$content->background) }}"  class="w-50 float-right pb-3 pl-2">
                          <div class="content-text text-justify font-size-base">
                            {!! $content->getTranslation("content",$lang) !!}
                          </div>
                        </div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('dashboard.pages.content.close') }}</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 mb-2">
                <div class="list-group">
                  @foreach($langs as $item)
                    <a href="{{ route('dashboard.content.edit',['content' => $content->id, 'lang' => $item->alias]) }}" class="list-group-item list-group-item d-flex justify-content-between align-items-center @if($lang === $item->alias) active @endif; ">
                      <i class="{{ $item->icon }} mr-1"></i> {{ $item->title }} <i class="feather @if($content->hasTranslation('content',$item->alias)) icon-check-circle @else icon-x-circle @endif"></i>
                    </a>
                  @endforeach
                </div>
              </div>
              @if(!empty($content->background))
                <div class="col-md-6 mb-2 d-flex align-items-center justify-content-center">
                  <p class="">
                    {{ __('dashboard.pages.content.background_image') }}
                  </p>
                </div>
                <div class="col-md-6 mb-2 text-center">
                  <img id="img-background" src="{{ asset("storage/$content->background") }}" class="w-50 img-thumbnail" alt="">
                </div>
              @endif

              <div class="col-12 mb-2">
                <div class="file-zone">
                  <div class="dropzone rounded mb-2" id="file-background"></div>
                  {!! __('dashboard.action.dropzone_hint') !!}
                </div>
                <input type="hidden" value="{{ $content->background }}" name="background" id="js__uploaded_background">
              </div>
              <div class="col-12 mb-2">
                <button type="submit" class="btn mb-1 btn-primary btn-lg btn-block waves-effect waves-light">
                  {{ __('dashboard.action.edit') }}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('vendor-script')
  <!-- vendor files -->

  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
  {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

  <script>
    Dropzone.autoDiscover = false;

    let dropzone_params =   {
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '{{ route("dashboard.file-upload") }}',
      uploadMultiple: false,
      maxFiles:1,
      maxFilesize: 3,
      acceptedFiles: "image/*",
      addRemoveLinks: true,
      dictDefaultMessage: "{!! __('dashboard.action.dropzone_placeholder') !!}",
      dictRemoveFile:"{!! __('dashboard.action.dropzone_delete') !!}",
      success: function (file,response){
        document.getElementById('js__uploaded_background').value = response.file

        console.log(response)
        if (response.status)
          Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
        else
          Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
      },
      removedfile: function (){

        let old_file = $('#js__uploaded_background').val()
        let data = {
          old : old_file
        }
        $.ajax({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: "{{ route('dashboard.file-delete') }}",
          method: 'delete',
          data,
          success:function (response){
            console.log(response)
          }
        })
        {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
      }
    }

    new Dropzone("#file-background",{...dropzone_params,...{
        paramName: "background",
        acceptedFiles: "image/*",
        params:{
          old: $('#js__uploaded_background').val()
        }
      }
    });


    ClassicEditor
      .create( document.querySelector( '#editor' ), {

        toolbar: {
          items: [
            'heading',
            'fontFamily',
            'fontSize',
            'fontColor',
            'fontBackgroundColor',
            '|',
            'bold',
            'italic',
            'underline',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            'alignment',
            'indent',
            'outdent',
            '|',
            'imageUpload',
            'imageInsert',
            'htmlEmbed',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            '|',
            'pageBreak',
            'horizontalLine',
            'MathType',
            'ChemType',
            'restrictedEditingException',
            'specialCharacters',
            '|',
            'undo',
            'redo'
          ]
        },
        language: 'ru',
        image: {
          styles: [
            'alignLeft', 'alignCenter', 'alignRight'
          ],
          resizeOptions: [
            {
              name: 'imageResize:original',
              label: 'Original',
              value: null
            },
            {
              name: 'imageResize:50',
              label: '50%',
              value: '50'
            },
            {
              name: 'imageResize:75',
              label: '75%',
              value: '75'
            }
          ],
          toolbar: [
            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
            '|',
            'imageResize',
            '|',
            'imageTextAlternative'
          ]
        },
        table: {
          contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells',
            'tableCellProperties',
            'tableProperties'
          ]
        },
        licenseKey: '',
      } )
      .then( editor => {
        window.editor = editor;
      } )
      .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
        console.error( error );
      } );
  </script>
@endsection


