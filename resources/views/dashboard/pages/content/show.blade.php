@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.content.create'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
  <form action="{{ route('dashboard.content.update',['content' => $content->id]) }}" method="POST" class="row was-validated">
    @csrf
    @method('PUT')
    <input type="hidden" name="type_id" value="{{ $content->type->id }}">
    <input type="hidden" name="url" value="">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12 mb-2">
                <x-html.input type="text" :value="$content->title" name="title" :label="__('dashboard.pages.content.input.title')" :required="true" :disabled="false"/>
              </div>
              <div class="col-12 mb-3">
                <textarea name="content" id="editor" style="min-width: 200px;">
                  {!! $content->getTranslation('content',app()->getLocale()) !!}
                </textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12 mb-2">
                <div class="card text-center border-primary">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="row align-items-center">
                        <div class="col"><i class="fal {{ $content->type->icon }} fa-5x text-primary"></i></div>
                        <div class="col">
                          <h2 class="card-title mt-1 text-primary">{{ $content->type->title }}</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 mb-2">
                <select data-placeholder="{{ __('dashboard.lang.select') }}..." name="lang" class="select2-icons form-control">
                  <optgroup label="{{ __('dashboard.lang.title') }}">
                    @foreach($lang as $item)
                      <option value="{{ $item->alias }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) selected @endif>{{ $item->title }}</option>
                    @endforeach
                  </optgroup>
                </select>
              </div>
              <div class="col-12 mb-2">
                <select data-placeholder="{{ __('dashboard.package.select') }}..." name="package_id" class="select2-icons form-control">
                  <optgroup label="{{ __('dashboard.package.title') }}">
                    @foreach($package as $item)
                      <option value="{{ $item->id }}" data-icon="{{ $item->icon }}" @if($item->id === $content->package->id) selected @endif>{{ $item->title }}</option>
                    @endforeach
                  </optgroup>
                </select>
              </div>
              <div class="col-12 mb-2">
                <div class="file-zone">
                  <div class="dropzone rounded mb-2" id="file-background"></div>
                  {!! __('dashboard.action.dropzone_hint') !!}
                </div>
                <input type="hidden" value="" name="background" id="js__uploaded_background">
              </div>
              <div class="col-12 mb-2">
                <button type="submit" class="btn btn-block btn-primary">
                  {{ __('dashboard.action.save') }}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('vendor-script')
  <!-- vendor files -->

  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
  {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

  <script>
    Dropzone.autoDiscover = false;

    let dropzone_params =   {
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '{{ route("dashboard.file-upload") }}',
      uploadMultiple: false,
      maxFiles:1,
      maxFilesize: 3,
      acceptedFiles: "image/*",
      addRemoveLinks: true,
      dictDefaultMessage: "{!! __('dashboard.action.dropzone_placeholder') !!}",
      dictRemoveFile:"{!! __('dashboard.action.dropzone_delete') !!}",
      success: function (file,response){
        document.getElementById('js__uploaded_background').value = response.file
        console.log(response)
        if (response.status)
          Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
        else
          Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
      },
      removedfile: function (){

        let old_file = $('#js__uploaded_background').val()
        let data = {
          old : old_file
        }
        $.ajax({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: "{{ route('dashboard.file-delete') }}",
          method: 'delete',
          data,
          success:function (response){
            console.log(response)
          }
        })
        {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
      }
    }

    new Dropzone("#file-background",{...dropzone_params,...{
        paramName: "background",
        acceptedFiles: "image/*",
        params:{
          old: $('#js__uploaded_background').val()
        }
      }
    });


    ClassicEditor
      .create( document.querySelector( '#editor' ), {

        toolbar: {
          items: [
            'heading',
            'fontFamily',
            'fontSize',
            'fontColor',
            'fontBackgroundColor',
            '|',
            'bold',
            'italic',
            'underline',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            'alignment',
            'indent',
            'outdent',
            '|',
            'imageUpload',
            'imageInsert',
            'htmlEmbed',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            '|',
            'pageBreak',
            'horizontalLine',
            'MathType',
            'ChemType',
            'restrictedEditingException',
            'specialCharacters',
            '|',
            'undo',
            'redo'
          ]
        },
        language: 'ru',
        image: {
          styles: [
            'alignLeft', 'alignCenter', 'alignRight'
          ],
          resizeOptions: [
            {
              name: 'imageResize:original',
              label: 'Original',
              value: null
            },
            {
              name: 'imageResize:50',
              label: '50%',
              value: '50'
            },
            {
              name: 'imageResize:75',
              label: '75%',
              value: '75'
            }
          ],
          toolbar: [
            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
            '|',
            'imageResize',
            '|',
            'imageTextAlternative'
          ]
        },
        table: {
          contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells',
            'tableCellProperties',
            'tableProperties'
          ]
        },
        licenseKey: '',
      } )
      .then( editor => {
        window.editor = editor;
      } )
      .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
        console.error( error );
      } );
  </script>
@endsection


