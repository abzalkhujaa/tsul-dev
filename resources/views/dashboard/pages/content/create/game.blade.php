@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.content.create'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
  <form action="{{ route('dashboard.content.store') }}" method="POST" class="row was-validated">
    @csrf
    <input type="hidden" name="type_id" value="{{ $type->id }}">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12 mb-2">
                <x-html.input type="text" name="title" :label="__('dashboard.pages.content.input.title')" :required="true" :disabled="false"/>
              </div>
              @error('url')
              <div class="col-12">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                  <i class="feather icon-star mr-1 align-middle"></i>
                  <span class="mb-0">
                   {{ $message }}
                  </span>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                  </button>
                </div>
              </div>
              @enderror
              <div class="col-md-6 mb-2">
                <h4>{{ __('dashboard.pages.content.input.url') }}:</h4>
                <div class="file-zone">
                  <div class="dropzone rounded mb-2" id="file-url"></div>
                  {!! __('dashboard.action.dropzone_hint') !!}
                </div>
                <input type="hidden" name="url" value="" id="js__file-url" required>
              </div>
              <div class="col-md-6 mb-2">
                <h4>{{ __('dashboard.pages.content.input.image') }}:</h4>
                <div class="file-zone">
                  <div class="dropzone rounded mb-2" id="file-image"></div>
                  {!! __('dashboard.action.dropzone_hint') !!}
                </div>
                <input type="hidden" name="image" value="" id="js__file-image" required>
              </div>
              <div class="col-12 mb-3">
                <textarea name="content" id="editor" style="min-width: 200px;"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="row">
              <div class="col-12 mb-2">
                <div class="card text-center border-primary">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="row align-items-center">
                        <div class="col"><i class="fal {{ $type->icon }} fa-5x text-primary"></i></div>
                        <div class="col">
                          <h2 class="card-title mt-1 text-primary">{{ $type->title }}</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 mb-2">
                <select data-placeholder="{{ __('dashboard.lang.select') }}..." name="lang" class="select2-icons form-control">
                  <optgroup label="{{ __('dashboard.lang.title') }}">
                    @foreach($lang as $item)
                      <option value="{{ $item->alias }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>
                    @endforeach
                  </optgroup>
                </select>
              </div>
              {{--              <div class="col-12 mb-2">--}}
              {{--                <select data-placeholder="{{ __('dashboard.package.select') }}..." name="package_id" class="select2-icons form-control">--}}
              {{--                  <optgroup label="{{ __('dashboard.package.title') }}">--}}
              {{--                    @foreach($package as $item)--}}
              {{--                      <option value="{{ $item->id }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>--}}
              {{--                    @endforeach--}}
              {{--                  </optgroup>--}}
              {{--                </select>--}}
              {{--              </div>--}}
              <div class="col-12 mb-2">
                <h4>{{ __('dashboard.pages.content.input.background') }}:</h4>
                <div class="file-zone">
                  <div class="dropzone rounded mb-2" id="file-background"></div>
                  {!! __('dashboard.action.dropzone_hint') !!}
                </div>
                <input type="hidden" value="" name="background" id="js__uploaded_background">
              </div>
              <div class="col-12 mb-2">
                <button type="submit" class="btn btn-block btn-primary">
                  {{ __('dashboard.action.save') }}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('vendor-script')
  <!-- vendor files -->

  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
  {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

  <script>
    Dropzone.autoDiscover = false;

    let dropzone_params =   {
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '{{ route("dashboard.file-upload") }}',
      uploadMultiple: false,
      maxFiles:1,
      maxFilesize: 3,
      addRemoveLinks: true,
      dictDefaultMessage: "{!! __('dashboard.action.dropzone_placeholder') !!}",
      dictRemoveFile:"{!! __('dashboard.action.dropzone_delete') !!}",
    }

    new Dropzone("#file-background",{...dropzone_params,...{
        paramName: "background",
        acceptedFiles: "image/*",
        params:{
          old: $('#js__uploaded_background').val()
        },
        success: function (file,response){
          document.getElementById('js__uploaded_background').value = response.file
          if (response.status)
            Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
          else
            Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
        },
        removedfile: function (file){

          let old_file = $('#js__uploaded_background').val()
          let data = {
            old : old_file
          }
          $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('dashboard.file-delete') }}",
            method: 'delete',
            data,
            success:function (response){
              $('#file-background .dz-preview').remove()
            }
          })
          {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
        }
      }
    });

    new Dropzone("#file-url",{...dropzone_params,...{
        paramName: "game",
        acceptedFiles: "application/zip, application/octet-stream",
        maxFilesize: 500,
        timeout:100000,
        params:{
          old: $('#js__file-url').val()
        },
        success: function (file,response){
          document.getElementById('js__file-url').value = response.file
          if (response.status)
            Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
          else
            Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
        },
        removedfile: function (file){

          let old_file = $('#js__file-url').val()
          let data = {
            old : old_file
          }
          $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('dashboard.file-delete') }}",
            method: 'delete',
            data,
            success:function (response){
              $('#file-url .dz-preview').remove()
            }
          })
          {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
        }
      }
    });


    new Dropzone("#file-image",{...dropzone_params,...{
        paramName: "image",
        acceptedFiles: "image/*",
        maxFilesize: 500,
        params:{
          old: $('#js__file-image').val()
        },
        success: function (file,response){
          document.getElementById('js__file-image').value = response.file
          if (response.status)
            Swal.fire("{{ __('dashboard.action.success') }}","{{ __('dashboard.action.dropzone_upload_success') }}","success")
          else
            Swal.fire("{{ __('dashboard.action.error') }}","{{__("dashboard.action.dropzone_upload_error")}}","error")
        },
        removedfile: function (file){

          let old_file = $('#js__file-image').val()
          let data = {
            old : old_file
          }
          $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{ route('dashboard.file-delete') }}",
            method: 'delete',
            data,
            success:function (response){
              $('#file-video .dz-preview').remove()
            }
          })
          {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
        }
      }
    });


    ClassicEditor
      .create( document.querySelector( '#editor' ), {

        toolbar: {
          items: [
            'heading',
            'fontFamily',
            'fontSize',
            'fontColor',
            'fontBackgroundColor',
            '|',
            'bold',
            'italic',
            'underline',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            'alignment',
            'indent',
            'outdent',
            '|',
            'imageUpload',
            'imageInsert',
            'htmlEmbed',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            '|',
            'pageBreak',
            'horizontalLine',
            'MathType',
            'ChemType',
            'restrictedEditingException',
            'specialCharacters',
            '|',
            'undo',
            'redo'
          ]
        },
        language: 'ru',
        image: {
          styles: [
            'alignLeft', 'alignCenter', 'alignRight'
          ],
          resizeOptions: [
            {
              name: 'imageResize:original',
              label: 'Original',
              value: null
            },
            {
              name: 'imageResize:50',
              label: '50%',
              value: '50'
            },
            {
              name: 'imageResize:75',
              label: '75%',
              value: '75'
            }
          ],
          toolbar: [
            'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
            '|',
            'imageResize',
            '|',
            'imageTextAlternative'
          ]
        },
        table: {
          contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells',
            'tableCellProperties',
            'tableProperties'
          ]
        },
        licenseKey: '',
      } )
      .then( editor => {
        window.editor = editor;
      } )
      .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
        console.error( error );
      } );
  </script>
@endsection


