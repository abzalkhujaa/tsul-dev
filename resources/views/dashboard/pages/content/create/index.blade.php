@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.content.create'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="card-content">
        <div class="row">
          @foreach($types as $type)
            <div class="col-md-4 col-sm-6 col-12">
              <a  href="{{ route('dashboard.content.create',['type' => $type->alias]) }}" class="card text-center border-primary type-btn">
                <div class="card-content">
                  <div class="card-body">
                    <div>
                      <i class="fal {{ $type->icon }} fa-5x text-primary"></i>
                    </div>
                    <h4 class="card-title mt-1">{{ $type->title }}</h4>
                  </div>
                </div>
              </a>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
@endsection




