@extends('dashboard.layouts.app')


@section('title', __('dashboard.pages.content.all'))

@section('vendor-style')
  {{-- vendor files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
{{--  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">--}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('page-style')
  {{-- Page css files --}}
{{--  <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">--}}
  <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
@endsection

@section('content')
  {{-- Data list view starts --}}
  <section id="data-list-view" class="data-list-view-header">
    <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
        <div class="btn-group dropdown actions-dropodown">
          <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __('dashboard.action.title') }}
          </button>
          <div class="dropdown-menu">
{{--            <a class="dropdown-item" href="#"><i class="feather icon-plus-circle"></i>{{ __('dashboard.action.add_package') }}</a>--}}
            <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>{{ __('dashboard.action.delete') }}</a>
          </div>
        </div>
        <div class="btn-group dropdown actions-dropodown">
          <a href="{{ route('dashboard.content.create') }}" class="btn btn-primary px-1 py-1 waves-effect waves-light">
            <i class="feather icon-plus"></i>{{ __('dashboard.pages.content.create') }}
          </a>
        </div>
      </div>
    </div>

    {{-- DataTable starts --}}
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
        <tr>
          <th></th>
          <th>{{ __('dashboard.pages.content.input.title') }}</th>
          <th>{{ __('dashboard.lang.title') }}</th>
          <th>{{ __('dashboard.pages.content.type') }}</th>
          <th>{{ __('dashboard.pages.content.input.created_at') }}</th>
          <th>{{ __('dashboard.action.title') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($contents as $content)
          <tr>
            <td></td>
            <td class="product-name">{{ $content->title }}</td>
            <td class="product-category">
              @foreach($lang as $one)
                <span>
                  <i class="{{ $one->icon }} ml-1"></i>
                  @if($content->type->alias === \App\Models\Type::VIDEO)
                    @if($content->hasTranslation('title',$one->alias) && $content->hasTranslation('url',$one->alias))
                      <i class="feather icon-check-circle text-success"></i>
                    @else
                      <i class="feather icon-x-circle text-danger"></i>
                    @endif
                  @else
                    @if($content->hasTranslation('title',$one->alias) && $content->hasTranslation('content',$one->alias))
                      <i class="feather icon-check-circle text-success"></i>
                    @else
                      <i class="feather icon-x-circle text-danger"></i>@endif
                  @endif
                </span>
              @endforeach
            </td>
            <td>
              <i class="fal {{ $content->type->icon }} mr-1"></i> {{ $content->type->title }}
            </td>
            <td>
              <i class="fal fa-calendar-alt text-primary"></i> {{ $content->created_at->format('d.m.Y H:s') }}
            </td>
            <td class="product-action">
              <a href="{{ route('dashboard.content.edit', [
                            'content' => $content->id,
                            'lang' => app()->getLocale()
                           ]) }}" class="text-primary"><i class="feather icon-edit"></i></a>
              <span id="js__delete_btn" onclick="testFunc('{{ route('dashboard.content.destroy',['content' => $content->id]) }}','{{$content->title}}')" data-id="{{ $content->id }}"><i class="feather icon-trash text-danger"></i></span>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    {{-- DataTable ends --}}
  </section>
  {{-- Data list view end --}}
@endsection
@section('vendor-script')
  {{-- vendor js files --}}
{{--  <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>--}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
@endsection
@section('page-script')
  {{-- Page js files --}}
{{--  <script src="{{ asset('js/scripts/ui/data-list-view.js') }}"></script>--}}
  <script>
    // init list view datatable
    var dataListView = $(".data-list-view").DataTable({
      responsive: false,
      columnDefs: [
        {
          orderable: true,
          targets: 0,
          checkboxes: { selectRow: true }
        }
      ],
      dom:
        '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
      oLanguage: {
        sLengthMenu: "_MENU_",
        sSearch: ""
      },
      aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
      select: {
        style: "multi"
      },
      order: [[1, "asc"]],
      bInfo: false,
      pageLength: 5,
      buttons: [],
      initComplete: function(settings, json) {
        $(".dt-buttons .btn").removeClass("btn-secondary")
      }
    });

    dataListView.on('draw.dt', function(){
      setTimeout(function(){
        if (navigator.userAgent.indexOf("Mac OS X") != -1) {
          $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
        }
      }, 50);
    });
    // To append actions dropdown before add new button
    var actionDropdown = $(".actions-dropodown")
    actionDropdown.insertBefore($(".top .actions .dt-buttons"))
    // Scrollbar
    if ($(".data-items").length > 0) {
      new PerfectScrollbar(".data-items", { wheelPropagation: false })
    }
    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
      $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }

    testFunc = (url,title) => {
      Swal.fire({
        title: '{{ __('dashboard.action.are_you_sure') }}',
        text: "{!! __('dashboard.action.dont_revert') !!}",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '{{__('dashboard.action.yes_delete_btn')}}',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        cancelButtonText: '{{ __('dashboard.action.no_delete_btn') }}',
        buttonsStyling: false,
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url,
            data:{
              "_token": '{{ csrf_token() }}'
            },
            type: 'delete',
            success:function (data){
              Swal.fire({
                type: "success",
                title: title,
                text: '{!! __('dashboard.action.you_deleted') !!}',
                confirmButtonClass: 'btn btn-success',
              })
              setTimeout(function (){
                location.reload()
              },2000)
            }
          })
        }
      })
    }
  </script>
@endsection
