@extends('dashboard.layouts.app')

@section('title', __('dashboard.post.edit'))

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fancybox/jquery.fancybox.min.css') }}">
    <style>
        [data-fancybox] img{
            width: 75%;
        }
    </style>
@endsection



@section('content')
    <form action="{{ route('dashboard.post.update',['post' => $post->id, 'lang' => $current_lang]) }}" method="POST" class="row was-validated">
        @csrf
        @method('PUT')
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-1">
                                <x-html.input type="text" name="title" :value="$post->getTranslation('title',$current_lang)" :label="__('dashboard.post.title')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-1">
                                <x-html.input type="text" name="alias" :pattern="'pattern=[a-z0-9\-]+'" :value="$post->alias" :label="__('dashboard.post.slug')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-2">
                                <x-html.input type="text" name="description" :value="$post->getTranslation('description',$current_lang)" :label="__('dashboard.post.description')" pattern="maxlength=100" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <textarea name="content" id="editor" style="min-width: 200px;">
                                      {!! $post->getTranslation('content',$current_lang) !!}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <div class="list-group">
                                    @foreach($langs as $lang)
                                        <a href="{{ route('dashboard.post.edit',['post' => $post->id, 'lang' => $lang->alias]) }}" class="list-group-item d-flex justify-content-between align-items-center @if($lang->alias === $current_lang) active @endif">
                                            <i class="flag-icon {{ $lang->icon }} mr-1"></i>
                                            <span>{{ $lang->title }}</span>
                                            @if($post->hasTranslation('title',$lang->alias) and $post->hasTranslation('content',$lang->alias))
                                                <i class="feather icon-check-circle @if($lang->alias === $current_lang) text-white @else text-success @endif "></i>
                                            @else
                                                <i class="feather icon-x-circle @if($lang->alias === $current_lang) text-white @else text-danger @endif "></i>
                                            @endif
                                        </a>
                                    @endforeach
                                </div>
                                <input type="hidden" name="lang" value="{{$current_lang}}">
                            </div>
                            <div class="col-12 mb-2">
                                <select data-placeholder="{{ __('dashboard.category.select') }}..." name="category_id" class="select2-icons form-control">
                                    <optgroup label="{{ __('dashboard.category.index') }}">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" data-icon="{{ $category->icon }}" @if($post->category_id === $category->id) selected @endif>{{ $category->getTranslation('title',$current_lang) }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-12 mb-2">
                                <select data-placeholder="{{ __('dashboard.author.select') }}..." name="user_id" class="select2-icons form-control">
                                    <optgroup label="{{ __('dashboard.author.index') }}">
                                        @foreach($authors as $author)
                                            <option value="{{ $author->id }}" data-icon="fas fa-user" @if($post->user_id === $author->id) selected @endif>{{ $author->fullName }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-12 mb-2">
                                <div class="form-group">
                                    <select data-placeholder="{{ __('dashboard.tag.select') }}..." name="tags[]" class="select2-icons form-control" id="multiple-select2-icons" multiple="multiple">
                                        <optgroup label="{{ __('dashboard.tag.index') }}">
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}" data-icon="fas fa-tag" @if(in_array($tag->id, $selected_tags)) selected @endif>
                                                    {{ $tag->getTranslation('name',app()->getLocale()) }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            @if(!is_null($post->image))
                                <div class="col-12 mb-2 text-center">
                                    <a href="{{ asset($post->image) }}" id="img_show_link" data-fancybox class="w-50">
                                        <img src="{{ asset($post->image) }}" id="img_show_item" alt="">
                                    </a>
                                </div>
                            @endif
                            <div class="col-12 mb-2">
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-background"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" value="{{ $post->image }}" name="image" id="js__uploaded_background">
                            </div>
                            <div class="col-12 mb-2">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('actions.update') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('vendor-script')
    <!-- vendor files -->

    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/fancybox/jquery.fancybox.min.js') }}"></script>
    {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("dashboard.file-upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('actions.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('actions.dropzone_delete') !!}",
            success: function (file,response){
                let base_url = '{{ asset('') }}';
                document.getElementById('js__uploaded_background').value = response.file
                document.getElementById('img_show_link').href = base_url + response.file
                document.getElementById('img_show_item').src = base_url + response.file
                if (response.status)
                    Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                else
                    Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
            },
            removedfile: function (){

                let old_file = $('#js__uploaded_background').val()
                let data = {
                    old : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('dashboard.file-delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }

        new Dropzone("#file-background",{...dropzone_params,...{
                paramName: "image",
                acceptedFiles: "image/*",
                params:{
                    old: $('#js__uploaded_background').val()
                }
            }
        });


        ClassicEditor
            .create( document.querySelector( '#editor' ), {
                toolbar: {
                    items: [
                        'heading',
                        'fontFamily',
                        'fontSize',
                        'fontColor',
                        'fontBackgroundColor',
                        '|',
                        'bold',
                        'italic',
                        'underline',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'alignment',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'imageInsert',
                        'htmlEmbed',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        '|',
                        'pageBreak',
                        'horizontalLine',
                        'MathType',
                        'ChemType',
                        'restrictedEditingException',
                        'specialCharacters',
                        '|',
                        'undo',
                        'redo'
                    ]
                },
                language: 'ru',
                image: {
                    styles: [
                        'alignLeft', 'alignCenter', 'alignRight'
                    ],
                    resizeOptions: [
                        {
                            name: 'imageResize:original',
                            label: 'Original',
                            value: null
                        },
                        {
                            name: 'imageResize:50',
                            label: '50%',
                            value: '50'
                        },
                        {
                            name: 'imageResize:75',
                            label: '75%',
                            value: '75'
                        }
                    ],
                    toolbar: [
                        'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                        '|',
                        'imageResize',
                        '|',
                        'imageTextAlternative'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells',
                        'tableCellProperties',
                        'tableProperties'
                    ]
                },
                licenseKey: '',
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
    </script>
@endsection


