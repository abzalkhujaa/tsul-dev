@extends('dashboard.layouts.app')


@section('title', __('dashboard.post.index'))

@section('vendor-style')
    {{-- vendor files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    {{--  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">--}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
@endsection

@section('content')
    {{-- Data list view starts --}}
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('actions.title') }}
                    </button>
                    <div class="dropdown-menu">
                        {{--            <a class="dropdown-item" href="#"><i class="feather icon-plus-circle"></i>{{ __('dashboard.action.add_package') }}</a>--}}
                        <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>{{ __('actions.delete') }}</a>
                    </div>
                </div>
                <div class="btn-group dropdown actions-dropodown">
          <a href="{{ route('dashboard.post.create') }}" class="btn btn-primary px-1 py-1 waves-effect waves-light js__add">
            <i class="feather icon-plus"></i>{{ __('dashboard.post.add') }}
          </a>
                </div>
            </div>
        </div>

        {{-- DataTable starts --}}
        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ __('dashboard.post.title') }}</th>
                    <th>{{ __('Language') }}</th>
                    <th>{{ __('dashboard.post.category') }}</th>
                    <th>{{ __('dashboard.tag.created_at') }}</th>
                    <th>{{ __('actions.title') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td></td>
                        <td title="{{ $post->title }}" data-toggle="tooltip">{{ Str::of($post->title)->limit(50) }}</td>
                        <td>
                            @foreach($langs as $lang)
                                <span>
                                    <i class="{{ $lang->icon }} ml-1"></i>
                                    @if($post->hasTranslation('title',$lang->alias))
                                        <i class="feather icon-check-circle text-success"></i>
                                    @else
                                        <i class="feather icon-x-circle text-danger"></i>
                                    @endif
                                </span>
                            @endforeach
                        </td>
                        <td>
                            <i class="{{ $post->category->icon }}"></i>
                            {{ $post->category->getTranslation('title',app()->getLocale()) }}
                        </td>
                        <td>
                            {{ $post->created_at->format("d.m.Y H:i") }}
                        </td>
                        <td>
                            <a href="{{ route('dashboard.post.edit',['post' => $post->id,'lang' => app()->getLocale()]) }}" class="text-primary">
                                <i class="feather icon-edit"></i>
                            </a>
                            <span class="text-danger" onclick="js_delete('{{route('dashboard.post.destroy',['post' => $post->id])}}')">
                                <i class="feather icon-trash"></i>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


        {{-- DataTable ends --}}
    </section>
    {{-- Data list view end --}}
@endsection
@section('vendor-script')
    {{-- vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection
@section('page-script')

    <script>

        // init list view datatable
        var dataListView = $(".data-list-view").DataTable({
            responsive: false,
            columnDefs: [
                {
                    orderable: true,
                    targets: 0,
                    checkboxes: { selectRow: true }
                }
            ],
            dom:
                '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            select: {
                style: "multi"
            },
            order: [[1, "asc"]],
            bInfo: false,
            pageLength: 10,
            buttons: [],
            initComplete: function(settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            }
        });

        dataListView.on('draw.dt', function(){
            setTimeout(function(){
                if (navigator.userAgent.indexOf("Mac OS X") != -1) {
                    $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
                }
            }, 50);
        });
        // To append actions dropdown before add new button
        var actionDropdown = $(".actions-dropodown")
        actionDropdown.insertBefore($(".top .actions .dt-buttons"))
        // Scrollbar
        if ($(".data-items").length > 0) {
            new PerfectScrollbar(".data-items", { wheelPropagation: false })
        }
        // mac chrome checkbox fix
        if (navigator.userAgent.indexOf("Mac OS X") != -1) {
            $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
        }

        function js_delete(route){
            Swal.fire({
                title: '{{ __('message.are_you_sure') }}',
                text: '{{ __('message.dont_revert') }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '{{ __('message.yes_delete') }}',
                cancelButtonText: '{{ __('message.cancel') }}',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    axios.delete(route).then(response => {
                        let data = response.data
                        if (data.status === 'success'){
                            Swal.fire({
                                title: data.message.title,
                                text: data.message.text,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok',
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                                buttonsStyling: false
                            }).then(function (result){
                                if (result.value){
                                    window.location.reload()
                                }else{

                                }
                            });
                        }
                    }).catch(response => {
                        Swal.fire({
                            type: 'error',
                            title: '{{ __('message.deleted') }}',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    })

                }
            });
        }

    </script>
@endsection
