@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.package.create'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/dragula.min.css')) }}">

@endsection
@section('page-style')
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/drag-and-drop.css')) }}">
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="card-content">
        <form action="{{ route('dashboard.package.store') }}" method="POST" class="card-body was-validated" id="js__add_package">
          @csrf
          <input type="hidden" name="type" value="multi">
          <div class="row">
            <div class="col-md-3 col-sm-4 col-6">
              <a href="{{ route('dashboard.package.create') }}" class="btn btn-block btn-outline-primary">
                <i class="fal fa-file mr-1"></i>{{ __('dashboard.pages.package.single') }}
              </a>
            </div>
            <div class="col-md-3 col-sm-4 col-6">
              <a href="{{ route('dashboard.package.create.multi') }}" class="btn btn-block btn-primary active">
                <i class="fal fa-copy mr-1"></i>{{ __('dashboard.pages.package.multi') }} <i class="feather icon-check"></i>
              </a>
            </div>
          </div>
          <ul class="nav nav-tabs justify-content-end" role="tablist">
            @foreach($lang as $one)
              <li class="nav-item">
                <a class="nav-link @if(app()->getLocale() === $one->alias) active @endif" id="package_link_{{$one->alias}}" data-toggle="tab" href="#package_{{$one->alias}}" role="tab" aria-selected="true">
                  <i class="{{ $one->icon }}"></i> {{ $one->title }}
                </a>
              </li>
            @endforeach
          </ul>
          @error('name')
          <div class="alert alert-danger alert-dismissible fade show my-2" role="alert">
            <h4 class="alert-heading">{{ __("validation.error") }}</h4>
            <p>
              {{ $message }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
            </button>
          </div>
          @enderror
          <div class="tab-content">
            @foreach($lang as $one)
              <div class="tab-pane @if(app()->getLocale() === $one->alias) active @else fade @endif" id="package_{{$one->alias}}" aria-labelledby="package_link_{{$one->alias}}" role="tabpanel">
                <div class="row">
                  <div class="col-12">
                    <x-html.input type="text" name="title[{{$one->alias}}]" :label="__('dashboard.pages.package.input.title')" :required="false" :disabled="false"/>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h2 class="card-title" style="font-size: 30px;">
                    {{ __('dashboard.pages.package.control') }}
                  </h2>
                </div>
                <div class="card-content">
                  <div class="card-body">
                    <p>{{ __('dashboard.pages.package.hint') }}</p>
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <h4 class="my-1"><i class="feather icon-layers mr-1"></i>{{ __('dashboard.pages.package.content') }}</h4>
                        <ul class="list-group list-group-flush" id="multiple-list-group-a"></ul>
                        <h4 class="my-1"><i class="feather icon-grid mr-1"></i>{{ __('dashboard.pages.package.multi') }} <i class="feather icon-"></i></h4>
                        <ul class="list-group list-group-flush" id="multiple-list-group-c"></ul>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <h4 class="my-1"><i class="feather icon-clipboard mr-1"></i>{{ __('dashboard.pages.content.all') }}</h4>
                        <ul class="list-group list-group-flush" id="multiple-list-group-b">
                          @foreach($contents as $content)
                            <li class="list-group-item" data-id="{{ $content->id }}">
                              <div class="media">
                                <div class="rounded-circle mr-2 bg-white shadow d-flex align-items-center justify-content-center" style="width: 50px;height: 50px;">
                                  <i class="fal {{$content->type->icon}} text-primary" style="font-size: 30px;"></i>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-0">{{ $content->getTranslation('title',app()->getLocale()) }}</h5>
                                  @foreach($lang as $one)
                                    <span>
                                      <i class="{{ $one->icon }} ml-1"></i>
                                      @if($content->type->alias === \App\Models\Type::VIDEO)
                                        @if($content->hasTranslation('title',$one->alias) && $content->hasTranslation('url',$one->alias))
                                          <i class="feather icon-check-circle text-success"></i>
                                        @else
                                          <i class="feather icon-x-circle text-danger"></i>
                                        @endif
                                      @else
                                        @if($content->hasTranslation('title',$one->alias) && $content->hasTranslation('content',$one->alias))
                                          <i class="feather icon-check-circle text-success"></i>
                                        @else
                                          <i class="feather icon-x-circle text-danger"></i>@endif
                                      @endif
                                    </span>
                                  @endforeach
                                </div>
                              </div>
                            </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <input type="hidden" name="content_ids" id="js__content_ids_input" required>
              <input type="hidden" name="multi_content_ids" id="js__multi_content_ids_input" required>
              <button type="button" class="btn btn-block btn-primary" id="js__package_content_btn" >Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/dragula.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
{{--  <script src="{{ asset('js/scripts/extensions/drag-drop.js') }}"></script>--}}
  <script>
    dragula([document.getElementById('multiple-list-group-a'), document.getElementById('multiple-list-group-b'), document.getElementById("multiple-list-group-c")]);

    function drag_content_ids(){
      let items = $('#multiple-list-group-a li')
      let ids = [];
      for (let item of items){
        ids.push( item.getAttribute('data-id'))
      }
      console.log(ids)
      // $('#js__content_ids_input').val(ids)
      console.log($('#js__content_ids_input'))
      // $('#js__add_package').submit()
    }
    $('#js__package_content_btn').click(function (){
      let items = $('#multiple-list-group-a li')
      let multi = $('#multiple-list-group-c li')
      let ids = [];
      let multi_ids = [];
      for (let item of items){
        ids.push( item.getAttribute('data-id'))
      }
      for (let multi_one of multi){
        multi_ids.push(multi_one.getAttribute('data-id'))
      }
      // console.log(ids)
      $('#js__content_ids_input').val(ids)
      $('#js__multi_content_ids_input').val(multi_ids)
      // console.log(multi_ids)
      $('#js__add_package').submit()
    });




  </script>
@endsection




