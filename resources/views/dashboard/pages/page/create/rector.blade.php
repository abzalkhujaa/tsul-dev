@extends('dashboard.layouts.app')

@section('title', __('dashboard.page.create.rector'))

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
    <form action="{{ route('dashboard.page.store',['type' => $type]) }}" method="POST" class="row was-validated">
        @csrf
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <x-html.input type="text" name="title" :label="__('dashboard.page.title')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="editor" class="text-dark font-weight-bold mb-1">{{ __('dashboard.page.rector_greeting') }}</label>
                                <textarea name="content" id="editor" style="min-width: 200px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="full_name" :label="__('dashboard.page.full_name')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="place" :label="__('dashboard.page.place')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="email" name="email" :label="__('dashboard.page.email')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="phone" :label="__('dashboard.page.phone')" :required="true" :disabled="false"/>
                            </div>

                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="telegram" :label="__('dashboard.page.telegram')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="twitter" :label="__('dashboard.page.twitter')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="facebook" :label="__('dashboard.page.facebook')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="editor-2" class="text-dark font-weight-bold mb-1">{{ __('dashboard.page.biography') }}</label>
                                <textarea name="biography" id="editor-2" style="min-width: 200px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <select data-placeholder="{{ __('dashboard.lang.select') }}..." name="lang" class="select2-icons form-control">
                                    <optgroup label="{{ __('dashboard.lang.index') }}">
                                        @foreach($langs as $item)
                                            <option value="{{ $item->alias }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-12 mb-2">
                                <label for="file-background">
                                    {{ __('dashboard.page.image') }}
                                </label>
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-background"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" value="" name="image" id="js__uploaded_background">
                            </div>

                            <div class="col-12 mb-2">
                                <label for="js__file-url">
                                    {{ __('dashboard.page.video') }}
                                </label>
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-video"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" name="video" value="" id="js__file-url">
                            </div>

                            <div class="col-12 mb-2">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('actions.create') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('vendor-script')
    <!-- vendor files -->

    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("dashboard.file-upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('actions.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('actions.dropzone_delete') !!}",
            success: function (file,response){
                document.getElementById('js__uploaded_background').value = response.file
                if (response.status)
                    Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                else
                    Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
            },
            removedfile: function (){

                let old_file = $('#js__uploaded_background').val()
                let data = {
                    old : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('dashboard.file-delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }

        new Dropzone("#file-background",{...dropzone_params,...{
                paramName: "image",
                acceptedFiles: "image/*",
                params:{
                    old: $('#js__uploaded_background').val()
                }
            }
        });

        new Dropzone("#file-video",{...dropzone_params,...{
                paramName: "video",
                acceptedFiles: "video/*",
                maxFilesize: 500,
                params:{
                    old: $('#js__file-url').val()
                },
                success: function (file,response){
                    document.getElementById('js__file-url').value = response.file
                    if (response.status)
                        Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                    else
                        Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
                },
                removedfile: function (file){

                    let old_file = $('#js__file-url').val()
                    let data = {
                        old : old_file
                    }
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{ route('dashboard.file-delete') }}",
                        method: 'delete',
                        data,
                        success:function (response){
                            $('#file-video .dz-preview').remove()
                        }
                    })
                    {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
                }
            }
        });



        let ckeditor_config = {
            toolbar: {
                items: [
                    'heading',
                    'fontFamily',
                    'fontSize',
                    'fontColor',
                    'fontBackgroundColor',
                    '|',
                    'bold',
                    'italic',
                    'underline',
                    'link',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'alignment',
                    'indent',
                    'outdent',
                    '|',
                    'imageUpload',
                    'imageInsert',
                    'htmlEmbed',
                    'blockQuote',
                    'insertTable',
                    'mediaEmbed',
                    '|',
                    'pageBreak',
                    'horizontalLine',
                    'MathType',
                    'ChemType',
                    'restrictedEditingException',
                    'specialCharacters',
                    '|',
                    'undo',
                    'redo'
                ]
            },
            language: 'ru',
            image: {
                styles: [
                    'alignLeft', 'alignCenter', 'alignRight'
                ],
                resizeOptions: [
                    {
                        name: 'imageResize:original',
                        label: 'Original',
                        value: null
                    },
                    {
                        name: 'imageResize:50',
                        label: '50%',
                        value: '50'
                    },
                    {
                        name: 'imageResize:75',
                        label: '75%',
                        value: '75'
                    }
                ],
                toolbar: [
                    'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                    '|',
                    'imageResize',
                    '|',
                    'imageTextAlternative'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn',
                    'tableRow',
                    'mergeTableCells',
                    'tableCellProperties',
                    'tableProperties'
                ]
            },
            licenseKey: '',
        }
        ClassicEditor
            .create( document.querySelector( '#editor' ), ckeditor_config )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
        ClassicEditor
            .create( document.querySelector( '#editor-2' ), ckeditor_config )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
    </script>
@endsection


