@extends('dashboard.layouts.app')

@section('title', __('dashboard.page.create.simple'))

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
@endsection

@section('content')
    <form action="{{ route('dashboard.page.store',['type' => $type]) }}" method="POST" class="row was-validated">
        @csrf
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <x-html.input type="text" name="title" :label="__('dashboard.page.title')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <textarea name="content" id="editor" style="min-width: 200px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <select data-placeholder="{{ __('dashboard.lang.select') }}..." name="lang" class="select2-icons form-control">
                                    <optgroup label="{{ __('dashboard.lang.index') }}">
                                        @foreach($langs as $item)
                                            <option value="{{ $item->alias }}" data-icon="{{ $item->icon }}" @if(app()->getLocale() === $item->alias) @endif>{{ $item->title }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-12 mb-2">
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-background"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" value="" name="image" id="js__uploaded_background">
                            </div>
                            <div class="col-12 mb-2">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('actions.create') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('vendor-script')
    <!-- vendor files -->

    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("dashboard.file-upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('actions.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('actions.dropzone_delete') !!}",
            success: function (file,response){
                document.getElementById('js__uploaded_background').value = response.file
                if (response.status)
                    Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                else
                    Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
            },
            removedfile: function (){

                let old_file = $('#js__uploaded_background').val()
                let data = {
                    old : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('dashboard.file-delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }

        new Dropzone("#file-background",{...dropzone_params,...{
                paramName: "image",
                acceptedFiles: "image/*",
                params:{
                    old: $('#js__uploaded_background').val()
                }
            }
        });


        ClassicEditor
            .create( document.querySelector( '#editor' ), {
                toolbar: {
                    items: [
                        'heading',
                        'fontFamily',
                        'fontSize',
                        'fontColor',
                        'fontBackgroundColor',
                        '|',
                        'bold',
                        'italic',
                        'underline',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'alignment',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'imageInsert',
                        'htmlEmbed',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        '|',
                        'pageBreak',
                        'horizontalLine',
                        'MathType',
                        'ChemType',
                        'restrictedEditingException',
                        'specialCharacters',
                        '|',
                        'undo',
                        'redo'
                    ]
                },
                language: 'ru',
                image: {
                    styles: [
                        'alignLeft', 'alignCenter', 'alignRight'
                    ],
                    resizeOptions: [
                        {
                            name: 'imageResize:original',
                            label: 'Original',
                            value: null
                        },
                        {
                            name: 'imageResize:50',
                            label: '50%',
                            value: '50'
                        },
                        {
                            name: 'imageResize:75',
                            label: '75%',
                            value: '75'
                        }
                    ],
                    toolbar: [
                        'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                        '|',
                        'imageResize',
                        '|',
                        'imageTextAlternative'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells',
                        'tableCellProperties',
                        'tableProperties'
                    ]
                },
                licenseKey: '',
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
    </script>
@endsection


