@extends('dashboard.layouts.app')

@section('title', __('dashboard.page.create.rector'))

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fancybox/jquery.fancybox.min.css') }}">

@endsection

@section('page-style')
    <style>
        [data-fancybox] img{
            width: 50%;
        }
        [data-fancybox] video{
            width: 50%;
        }

    </style>
@endsection

@section('content')
    <form action="{{ route('dashboard.page.update',['page' => $page->id]) }}" method="POST" class="row was-validated">
        @csrf
        @method('PUT')
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <x-html.input type="text" name="title" :value="$page->getTranslation('title',$current_lang->alias)" :label="__('dashboard.page.title')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="editor" class="text-dark font-weight-bold mb-1">{{ __('dashboard.page.rector_greeting') }}</label>
                                <textarea name="content" id="editor" style="min-width: 200px;">{!! $page->contentJson($current_lang->alias)->get('content') !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="full_name" :value="$page->contentJson($current_lang->alias)->get('full_name') ?? ''" :label="__('dashboard.page.full_name')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="place" :value="$page->contentJson($current_lang->alias)->get('place') ?? ''" :label="__('dashboard.page.place')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="email" name="email" :value="$page->contentJson($current_lang->alias)->get('email') ?? ''" :label="__('dashboard.page.email')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-6 mb-1">
                                <x-html.input type="text" name="phone" :value="$page->contentJson($current_lang->alias)->get('phone') ?? ''" :label="__('dashboard.page.phone')" :required="true" :disabled="false"/>
                            </div>

                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="telegram" :value="$page->contentJson($current_lang->alias)->get('telegram') ?? ''" :label="__('dashboard.page.telegram')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="twitter" :value="$page->contentJson($current_lang->alias)->get('twitter') ?? ''" :label="__('dashboard.page.twitter')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-md-4 mb-1">
                                <x-html.input type="text" name="facebook" :value="$page->contentJson($current_lang->alias)->get('facebook') ?? ''" :label="__('dashboard.page.facebook')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="editor-2" class="text-dark font-weight-bold mb-1">{{ __('dashboard.page.biography') }}</label>
                                <textarea name="biography" id="editor-2" style="min-width: 200px;">{!! $page->contentJson($current_lang->alias)->get('biography') ?? '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <div class="list-group">
                                    @foreach($langs as $lang)
                                        <a href="{{ route('dashboard.page.edit',['page' => $page->id, 'lang' => $lang->alias]) }}" class="list-group-item d-flex justify-content-between align-items-center @if($lang->alias === $current_lang->alias) active @endif">
                                            <i class="flag-icon {{ $lang->icon }} mr-1"></i>
                                            <span>{{ $lang->title }}</span>
                                            @if($page->hasTranslation('title',$lang->alias) and $page->hasTranslation('content',$lang->alias))
                                                <i class="feather icon-check-circle @if($lang->alias === $current_lang->alias) text-white @else text-success @endif "></i>
                                            @else
                                                <i class="feather icon-x-circle @if($lang->alias === $current_lang->alias) text-white @else text-danger @endif "></i>
                                            @endif
                                        </a>
                                    @endforeach
                                </div>
                                <input type="hidden" name="lang" value="{{$current_lang->alias}}">
                            </div>
                            @if(!is_null($page->image))
                                <div class="col-12 mb-2 text-center">
                                    <a href="{{ asset($page->image) }}" id="img_show_link" data-fancybox class="w-50">
                                        <img src="{{ asset($page->image) }}" id="img_show_item" alt="">
                                    </a>
                                </div>
                            @endif
                            <div class="col-12 mb-2">
                                <label for="file-background">
                                    {{ __('dashboard.page.image') }}
                                </label>
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-background"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" value="{{ $page->image }}" name="image" id="js__uploaded_background">
                            </div>
                                <div class="col-12 mb-2 text-center">
                                    <a data-fancybox href="{{ asset($page->contentJson($current_lang->alias)->get('video')) }}" id="video_show_link">
                                        <span>
                                            <video id="video_show_item">
                                                <source src="{{ asset($page->contentJson($current_lang->alias)->get('video')) ?? '' }}"  type="video/mp4">
                                            </video>
                                        </span>
                                    </a>
                                </div>
                            <div class="col-12 mb-2">
                                <label for="js__file-url">
                                    {{ __('dashboard.page.video') }}
                                </label>
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-video"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" name="video" value="{{ $page->contentJson($current_lang->alias)->get('video') }} ?? ''" id="js__file-url">
                            </div>

                            <div class="col-12 mb-2">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('actions.update') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('vendor-script')
    <!-- vendor files -->

    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/fancybox/jquery.fancybox.min.js') }}"></script>
    {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("dashboard.file-upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('actions.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('actions.dropzone_delete') !!}",
            success: function (file,response){

                let base_url = '{{ asset('') }}';
                document.getElementById('js__uploaded_background').value = response.file
                document.getElementById('img_show_link').href = base_url + response.file
                document.getElementById('img_show_item').src = base_url + response.file

                if (response.status)
                    Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                else
                    Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
            },
            removedfile: function (){

                let old_file = $('#js__uploaded_background').val()
                let data = {
                    old : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('dashboard.file-delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }

        new Dropzone("#file-background",{...dropzone_params,...{
                paramName: "image",
                acceptedFiles: "image/*",
                params:{
                    old: $('#js__uploaded_background').val()
                }
            }
        });

        new Dropzone("#file-video",{...dropzone_params,...{
                paramName: "video",
                acceptedFiles: "video/*",
                maxFilesize: 500,
                params:{
                    old: $('#js__file-url').val()
                },
                success: function (file,response){
                    let base_url = '{{ asset('') }}';
                    document.getElementById('js__file-url').value = response.file
                    document.getElementById('video_show_link').href = base_url + response.file
                    document.getElementById('video_show_item').src = base_url + response.file

                    if (response.status)
                        Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                    else
                        Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
                },
                removedfile: function (file){

                    let old_file = $('#js__file-url').val()
                    let data = {
                        old : old_file
                    }
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{ route('dashboard.file-delete') }}",
                        method: 'delete',
                        data,
                        success:function (response){
                            $('#file-video .dz-preview').remove()
                        }
                    })
                    {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
                }
            }
        });



        let ckeditor_config = {
            toolbar: {
                items: [
                    'heading',
                    'fontFamily',
                    'fontSize',
                    'fontColor',
                    'fontBackgroundColor',
                    '|',
                    'bold',
                    'italic',
                    'underline',
                    'link',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'alignment',
                    'indent',
                    'outdent',
                    '|',
                    'imageUpload',
                    'imageInsert',
                    'htmlEmbed',
                    'blockQuote',
                    'insertTable',
                    'mediaEmbed',
                    '|',
                    'pageBreak',
                    'horizontalLine',
                    'MathType',
                    'ChemType',
                    'restrictedEditingException',
                    'specialCharacters',
                    '|',
                    'undo',
                    'redo'
                ]
            },
            language: 'ru',
            image: {
                styles: [
                    'alignLeft', 'alignCenter', 'alignRight'
                ],
                resizeOptions: [
                    {
                        name: 'imageResize:original',
                        label: 'Original',
                        value: null
                    },
                    {
                        name: 'imageResize:50',
                        label: '50%',
                        value: '50'
                    },
                    {
                        name: 'imageResize:75',
                        label: '75%',
                        value: '75'
                    }
                ],
                toolbar: [
                    'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                    '|',
                    'imageResize',
                    '|',
                    'imageTextAlternative'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn',
                    'tableRow',
                    'mergeTableCells',
                    'tableCellProperties',
                    'tableProperties'
                ]
            },
            licenseKey: '',
        }
        ClassicEditor
            .create( document.querySelector( '#editor' ), ckeditor_config )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
        ClassicEditor
            .create( document.querySelector( '#editor-2' ), ckeditor_config )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
    </script>
@endsection


