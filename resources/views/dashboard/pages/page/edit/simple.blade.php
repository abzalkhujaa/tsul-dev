@extends('dashboard.layouts.app')

@section('title', __('dashboard.page.create.simple'))

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fancybox/jquery.fancybox.min.css') }}">

@endsection

@section('page-style')
    <style>
        [data-fancybox] img{
            width: 75%;
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('dashboard.page.update',['page' => $page]) }}" method="POST" class="row was-validated">
        @csrf
        @method('PUT')
        <input type="hidden" name="lang" value="$current_lang->alias">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <x-html.input type="text" :value="$page->getTranslation('title',$current_lang->alias)" name="title" :label="__('dashboard.page.title')" :required="true" :disabled="false"/>
                            </div>
                            <div class="col-12 mb-3">
                                <textarea name="content" id="editor" style="min-width: 200px;">
                                    {!! $page->getTranslation('content',$current_lang->alias) !!}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <div class="list-group">
                                    @foreach($langs as $lang)
                                        <a href="{{ route('dashboard.page.edit',['page' => $page->id, 'lang' => $lang->alias]) }}" class="list-group-item d-flex justify-content-between align-items-center @if($lang->alias === $current_lang->alias) active @endif">
                                            <i class="flag-icon {{ $lang->icon }} mr-1"></i>
                                            <span>{{ $lang->title }}</span>
                                            @if($page->hasTranslation('title',$lang->alias) and $page->hasTranslation('content',$lang->alias))
                                                <i class="feather icon-check-circle @if($lang->alias === $current_lang->alias) text-white @else text-success @endif "></i>
                                            @else
                                                <i class="feather icon-x-circle @if($lang->alias === $current_lang->alias) text-white @else text-danger @endif "></i>
                                            @endif
                                        </a>
                                    @endforeach
                                </div>
                                <input type="hidden" name="lang" value="{{$current_lang->alias}}">
                            </div>
                            @if(!is_null($page->image))
                                <div class="col-12 mb-2 text-center">
                                    <a href="{{ asset($page->image) }}" id="img_show_link" data-fancybox class="w-50">
                                        <img src="{{ asset($page->image) }}" id="img_show_item" alt="">
                                    </a>
                                </div>
                            @endif
                            <div class="col-12 mb-2">
                                <div class="file-zone">
                                    <div class="dropzone rounded mb-2" id="file-background"></div>
                                    {!! __('actions.dropzone_hint') !!}
                                </div>
                                <input type="hidden" value="{{ $page->image }}" name="image" id="js__uploaded_background">
                            </div>
                            <div class="col-12 mb-2">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ __('actions.update') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('vendor-script')
    <!-- vendor files -->

    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/fancybox/jquery.fancybox.min.js') }}"></script>
    {{--  <script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>--}}

@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset('vendor/dropzone/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        let dropzone_params =   {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ route("dashboard.file-upload") }}',
            uploadMultiple: false,
            maxFiles:1,
            maxFilesize: 3,
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            dictDefaultMessage: "{!! __('actions.dropzone_placeholder') !!}",
            dictRemoveFile:"{!! __('actions.dropzone_delete') !!}",
            success: function (file,response){
                let base_url = '{{ asset('') }}';
                document.getElementById('js__uploaded_background').value = response.file
                document.getElementById('img_show_link').href = base_url + response.file
                document.getElementById('img_show_item').src = base_url + response.file
                if (response.status)
                    Swal.fire("{{ __('actions.success') }}","{{ __('actions.dropzone_upload_success') }}","success")
                else
                    Swal.fire("{{ __('actions.error') }}","{{__("actions.dropzone_upload_error")}}","error")
            },
            removedfile: function (){

                let old_file = $('#js__uploaded_background').val()
                let data = {
                    old : old_file
                }
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('dashboard.file-delete') }}",
                    method: 'delete',
                    data,
                    success:function (response){
                        $('.dz-preview').remove()
                    }
                })
                {{--axios.delete("{{ route('dashboard.file-delete') }}",data)--}}
            }
        }

        new Dropzone("#file-background",{...dropzone_params,...{
                paramName: "image",
                acceptedFiles: "image/*",
                params:{
                    old: $('#js__uploaded_background').val()
                }
            }
        });


        ClassicEditor
            .create( document.querySelector( '#editor' ), {
                toolbar: {
                    items: [
                        'heading',
                        'fontFamily',
                        'fontSize',
                        'fontColor',
                        'fontBackgroundColor',
                        '|',
                        'bold',
                        'italic',
                        'underline',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'alignment',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'imageInsert',
                        'htmlEmbed',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        '|',
                        'pageBreak',
                        'horizontalLine',
                        'MathType',
                        'ChemType',
                        'restrictedEditingException',
                        'specialCharacters',
                        '|',
                        'undo',
                        'redo'
                    ]
                },
                language: 'ru',
                image: {
                    styles: [
                        'alignLeft', 'alignCenter', 'alignRight'
                    ],
                    resizeOptions: [
                        {
                            name: 'imageResize:original',
                            label: 'Original',
                            value: null
                        },
                        {
                            name: 'imageResize:50',
                            label: '50%',
                            value: '50'
                        },
                        {
                            name: 'imageResize:75',
                            label: '75%',
                            value: '75'
                        }
                    ],
                    toolbar: [
                        'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                        '|',
                        'imageResize',
                        '|',
                        'imageTextAlternative'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells',
                        'tableCellProperties',
                        'tableProperties'
                    ]
                },
                licenseKey: '',
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something went wrong!' );
                console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
                console.warn( 'Build id: 7d6fcyaa37dn-l198vzv2nyq7' );
                console.error( error );
            } );
    </script>
@endsection


