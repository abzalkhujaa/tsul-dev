@extends('dashboard.layouts.app')

@section('title', __("dashboard.pages.dashboard.title"))

@section('content')
  <div class="container-fluid">
    <div class="row">
      @foreach($zones as $zone)
        <div class="col-md-4 mb-2">
          <div class="card h-100">
            <div class="card-header">
              <h1 class="card-title">{{ $zone->title }}</h1>
            </div>
            <div class="card-body">
              <div class="card-content">
                <div class="row justify-content-center">
                  @if($zone->terminal)
                    @foreach($zone->terminal as $terminal)
                      <div class="col-md-3 col-sm-6 col-6 text-center">
                        <a href="{{ route('dashboard.terminal.show',['terminal' => $terminal->id] ) }}"
                           class="@if($terminal->isWorking()) text-success @else text-danger @endif"
                        >
                          <i class="feather icon-monitor fa-5x" data-toggle="tooltip"
                             data-original-title="{{ ($terminal->last_active_at) ? $terminal->last_active_at->format('d.m.Y H:i') : __('dashboard.pages.terminal.not_working') }}" data-placement="bottom"></i><br>
                          <span class="text-dark font-weight-bold">{{ $terminal->name }}</span>
                        </a>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>

@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tooltip/tooltip.js')) }}"></script>
@endsection





