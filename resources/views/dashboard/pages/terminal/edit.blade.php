@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.terminal.edit'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="card-content">
        <form action="{{ route('dashboard.terminal.update',['terminal' => $terminal->id]) }}" method="POST"
              class="card-body was-validated">
          @csrf
          @method('put')
          <ul class="nav nav-tabs justify-content-end" role="tablist">
            @foreach($lang as $one)
              <li class="nav-item">
                <a class="nav-link @if(app()->getLocale() === $one->alias) active @endif"
                   id="terminal_link_{{$one->alias}}" data-toggle="tab" href="#terminal_{{$one->alias}}" role="tab"
                   aria-selected="true">
                  <i class="{{ $one->icon }}"></i> {{ $one->title }}
                </a>
              </li>
            @endforeach
          </ul>
          @error('name')
          <div class="alert alert-danger alert-dismissible fade show my-2" role="alert">
            <h4 class="alert-heading">{{ __("validation.error") }}</h4>
            <p>
              {{ $message }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
            </button>
          </div>
          @enderror
          <div class="tab-content">
            @foreach($lang as $one)
              <div class="tab-pane @if(app()->getLocale() === $one->alias) active @else fade @endif"
                   id="terminal_{{$one->alias}}" aria-labelledby="terminal_link_{{$one->alias}}" role="tabpanel">
                <div class="row">
                  <div class="col-12">
                    <x-html.input type="text" name="name[{{$one->alias}}]"
                                  :value="$terminal->getTranslation('name',$one->alias)"
                                  :label="__('dashboard.pages.terminal.input.name')" :required="false"
                                  :disabled="false"/>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="row mt-md-3 mt-sm-2 mt-1">
            <div class="col-md-4">
              <h2 class="mb-2"><i
                  class="feather icon-log-in text-primary"></i> {{ __('dashboard.pages.terminal.login_data') }}</h2>
              <x-html.input
                type="text"
                :value="$terminal->user->username"
                name="username"
                :label="__('dashboard.pages.terminal.input.login')"
                :required="true"
                :disabled="true"
                pattern="pattern=^[A-za-z0-9_]{0,15}$"/>
              <h2 class="mb-2"><i
                  class="feather icon-lock text-primary"></i> {{ __('dashboard.pages.terminal.new_password') }}</h2>
              <button type="button" class="btn btn-block btn-primary" data-toggle="modal"
                      data-target="#js__terminal_edit_password">{{ __('dashboard.pages.terminal.edit_password') }}</button>

              {{--              <x-html.input type="password" name="password" :label="__('dashboard.pages.terminal.input.password')" :required="false" :disabled="false" pattern="length=8"/>--}}
              {{--              <x-html.input type="password" name="password_confirmation" :label="__('dashboard.pages.terminal.input.c_password')" :required="false" :disabled="false" pattern="length=8"/>--}}
            </div>
            <div class="col-md-4 mb-md-0 mb-2">
              <h2 class="mb-2"><i
                  class="fal fa-map-marked-alt text-primary"></i> {{ __('dashboard.pages.terminal.terminal_zones') }}
              </h2>
              <select class="select2 form-control" name="zone_id" required>
                <option value="">{{ __('dashboard.pages.terminal.choose_zone') }}...</option>
                @foreach($zones as $zone)
                  <option value="{{ $zone->id }}"
                          @if($terminal->zone_id === $zone->id) selected @endif>{{ $zone->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-4 mb-md-0 mb-2">
              <h2 class="mb-2"><i
                  class="feather icon-layers text-primary"></i> {{ __('dashboard.pages.terminal.terminal_package') }}
              </h2>
              <select class="select2 form-control" name="package_id">
                <option value="">{{ __('dashboard.pages.terminal.choose_package') }}...</option>
                @foreach($packages as $package)
                  <option value="{{ $package->id }}"
                          @if($terminal->package_id === $package->id) selected @endif>{{ $package->title }}</option>
                @endforeach
              </select>
            </div>

            <div class="col-12 my-2 text-right">
              <button type="submit" class="btn btn-primary">
                {{ __("dashboard.action.edit") }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade text-left"
       id="js__terminal_edit_password"
       tabindex="-1"
       role="dialog"
       aria-labelledby="myModalLabel33"
       aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel33">{{ __('dashboard.pages.terminal.new_password') }} </h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="{{ route('dashboard.terminal.password-reset',['terminal' => $terminal->id]) }}" method="post" class="was-validated">
          @csrf
          @method('put')
          <div class="modal-body">
            <x-html.input type="password" name="password" :label="__('dashboard.pages.terminal.input.password')"
                          :required="true" :disabled="false"/>
            <x-html.input type="password" name="password_confirmation"
                          :label="__('dashboard.pages.terminal.input.c_password')" :required="true" :disabled="false"/>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">{{ __('dashboard.action.edit') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  @if(Session::get('success'))
    <script>
      Swal.fire('Password','{{ Session::get('success') }}','success')
    </script>
  @endif
  @error('password')
  <script>
    Swal.fire('Password','{{ $message }}','error')
  </script>
  @enderror
@endsection




