@extends('dashboard.layouts.app')

@section('title', $terminal->user->username)

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <div class="card-content">
            <div class="text-center my-2">
              <i class="fal fa-desktop fa-5x @if($terminal->isWorking()) text-primary @else text-danger @endif"></i>
            </div>
            <div class="row mt-4">
              <div class="col-6 text-right border-right font-weight-bold">{{ __('dashboard.pages.terminal.input.last_active_at') }}</div>
              <div class="col-6 text-left @if($terminal->isWorking()) text-dark @else text-danger @endif"><i class="fal fa-clock mr-1"></i><small>{{ $terminal->last_active_at ?? __('dashboard.pages.terminal.not_working') }}</small></div>
              <div class="col-12 divider"></div>
              <div class="col-6 text-right border-right font-weight-bold">{{ __('dashboard.pages.terminal.input.last_cached_at') }}</div>
              <div class="col-6 text-left @if($terminal->isWorking()) text-dark @else text-danger @endif">
                @if(!is_null($terminal->last_cached_at))
                  <i class="fal fa-clock text-primary mr-1"></i><small>{{ $terminal->last_cached_at->format('d.m.Y H:i') }}</small>
                @else
                  <span class="text-warning"><i class="fal fa-calendar-times mr-1"></i>{{ __("dashboard.pages.terminal.not_cached") }}</span>
                @endif
              </div>
              <div class="col-12 divider"></div>
              <div class="col-6 text-right border-right font-weight-bold">{{ __('dashboard.pages.terminal.input.last_session_at') }}</div>
              <div class="col-6 text-left">
                @if(!is_null($terminal->last_session_at))
                  <i class="fal fa-clock text-primary mr-1"></i> <small>{{ $terminal->last_session_at->format('d.m.Y H:i') }}</small>
                @else
                  <span class="text-warning"><i class="fal fa-calendar-times mr-1"></i>{{ __("dashboard.pages.terminal.unauthorized") }}</span>
                @endif
              </div>
              <div class="col-12 divider"></div>
              <div class="col-6 text-center mb-2">
                <a href="{{ route('dashboard.terminal.edit',['terminal' => $terminal->id]) }}" class="btn btn-primary">
                  <i class="feather icon-edit mr-1"></i> {{ __('dashboard.pages.terminal.edit') }}
                </a>
              </div>
              <div class="col-6 text-center mb-2">
                @if($terminal->package)
                  <a href="{{ route('dashboard.package.edit',['package' => $terminal->package->id]) }}" class="btn btn-primary">
                    <i class="feather icon-layers mr-1"></i> {{ __('dashboard.pages.package.edit') }}
                  </a>
                @else
                  <a href="#" class="btn btn-primary disabled" title="{{ __('dashboard.pages.terminal.without_package') }}">
                    <i class="fas fa-times mr-1"></i> {{ __('dashboard.pages.package.edit') }}
                  </a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
@endsection




