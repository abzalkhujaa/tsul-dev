@extends('dashboard.layouts.app')

@section('title', __('dashboard.pages.terminal.create'))

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="card-content">
        <form action="{{ route('dashboard.terminal.store') }}" method="POST" class="card-body was-validated">
          @csrf
          <ul class="nav nav-tabs justify-content-end" role="tablist">
            @foreach($lang as $one)
              <li class="nav-item">
                <a class="nav-link @if(app()->getLocale() === $one->alias) active @endif" id="terminal_link_{{$one->alias}}" data-toggle="tab" href="#terminal_{{$one->alias}}" role="tab" aria-selected="true">
                  <i class="{{ $one->icon }}"></i> {{ $one->title }}
                </a>
              </li>
            @endforeach
          </ul>
          @error('name')
          <div class="alert alert-danger alert-dismissible fade show my-2" role="alert">
            <h4 class="alert-heading">{{ __("validation.error") }}</h4>
            <p>
              {{ $message }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
            </button>
          </div>
          @enderror
          <div class="tab-content">
            @foreach($lang as $one)
              <div class="tab-pane @if(app()->getLocale() === $one->alias) active @else fade @endif" id="terminal_{{$one->alias}}" aria-labelledby="terminal_link_{{$one->alias}}" role="tabpanel">
                <div class="row">
                  <div class="col-12">
                    <x-html.input type="text" name="name[{{$one->alias}}]" :label="__('dashboard.pages.terminal.input.name')" :required="false" :disabled="false"/>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="row mt-md-3 mt-sm-2 mt-1">
            <div class="col-md-4">
              <h2 class="mb-2"><i class="fal fa-sign-in text-primary"></i> {{ __('dashboard.pages.terminal.login_data') }}</h2>
              <x-html.input type="text" name="username" :label="__('dashboard.pages.terminal.input.login')" :required="true" :disabled="false" pattern="pattern=^[A-za-z0-9_]{0,15}$"/>
              <x-html.input type="password" name="password" :label="__('dashboard.pages.terminal.input.password')" :required="true" :disabled="false" pattern="length=8"/>
              <x-html.input type="password" name="password_confirmation" :label="__('dashboard.pages.terminal.input.c_password')" :required="true" :disabled="false" pattern="length=8"/>
            </div>
            <div class="col-md-4 mb-md-0 mb-2">
              <h2 class="mb-2"><i class="fal fa-map-marked-alt text-primary"></i> {{ __('dashboard.pages.terminal.terminal_zones') }}</h2>
              <select class="select2 form-control" name="zone_id" required>
                <option value="">{{ __('dashboard.pages.terminal.choose_zone') }}...</option>
                @foreach($zones as $zone)
                  <option value="{{ $zone->id }}">{{ $zone->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-4 mb-md-0 mb-2">
              <h2 class="mb-2"><i class="fal fa-database text-primary"></i> {{ __('dashboard.pages.terminal.terminal_package') }}</h2>
              <select class="select2 form-control" name="package_id">
                <option value="">{{ __('dashboard.pages.terminal.choose_zone') }}...</option>
                @foreach($packages as $package)
                  <option value="{{ $package->id }}">{{ $package->title }}</option>
                @endforeach
              </select>
            </div>

            <div class="col-12 my-2 text-right">
              <button type="submit" class="btn btn-primary">
                {{ __("dashboard.action.add") }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script>
    DecoupledDocumentEditor
      .create( document.querySelector( '.editor' ), {

        toolbar: {
          items: [
            'heading',
            '|',
            'fontSize',
            'fontFamily',
            '|',
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'highlight',
            '|',
            'alignment',
            '|',
            'numberedList',
            'bulletedList',
            '|',
            'indent',
            'outdent',
            '|',
            'todoList',
            'link',
            'blockQuote',
            'imageUpload',
            'insertTable',
            'mediaEmbed',
            '|',
            'undo',
            'redo'
          ]
        },
        language: '{{app()->getLocale()}}',
        image: {
          toolbar: [
            'imageTextAlternative',
            'imageStyle:full',
            'imageStyle:side',
            'linkImage'
          ]
        },
        table: {
          contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells'
          ]
        },
        licenseKey: '',

      } )
      .then( editor => {
        window.editor = editor;
        // Set a custom container for the toolbar.
        document.querySelector( '.document-editor__toolbar' ).appendChild( editor.ui.view.toolbar.element );
        document.querySelector( '.ck-toolbar' ).classList.add( 'ck-reset_all' );
      } )
      .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: cyiobk11rpj7-ucn1dsls94e0' );
        console.error( error );
      } );
  </script>
  @error('username')
  <script>
    Swal.fire('Error!','{{ $message }}','error');
  </script>
  @enderror
@endsection




