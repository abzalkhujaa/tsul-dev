@extends('dashboard.layouts.app')


@section('title', __('dashboard.tag.index'))

@section('vendor-style')
  {{-- vendor files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  {{--  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">--}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('page-style')
  {{-- Page css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
@endsection

@section('content')
  {{-- Data list view starts --}}
  <section id="data-list-view" class="data-list-view-header">
    <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
        <div class="btn-group dropdown actions-dropodown">
          <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __('actions.title') }}
          </button>
          <div class="dropdown-menu">
            {{--            <a class="dropdown-item" href="#"><i class="feather icon-plus-circle"></i>{{ __('dashboard.action.add_package') }}</a>--}}
            <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>{{ __('actions.delete') }}</a>
          </div>
        </div>
        <div class="btn-group dropdown actions-dropodown">
          <span href="#" class="btn btn-primary px-1 py-1 waves-effect waves-light js__add">
            <i class="feather icon-plus"></i>{{ __('dashboard.tag.add') }}
          </span>
        </div>
      </div>
    </div>

    {{-- DataTable starts --}}
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
        <tr>
          <th></th>
          <th>{{ __('dashboard.tag.title') }}</th>
          <th>{{ __('Language') }}</th>
          <th>{{ __('dashboard.tag.created_at') }}</th>
          <th>{{ __('actions.title') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)
          <tr>
            <td></td>
            <td>{{ $tag->name }}</td>
            <td>
              @foreach($langs as $lang)
                <span>
                    <i class="{{ $lang->icon }} ml-1"></i>
                      @if($tag->hasTranslation('name',$lang->$lang))
                    <i class="feather icon-check-circle text-success"></i>
                  @else
                    <i class="feather icon-x-circle text-danger"></i>
                  @endif
                  </span>
              @endforeach
            </td>
            <td>
              {{ $tag->created_at->format("d.m.Y H:i") }}
            </td>
            <td>
                <span
                        class="text-primary js__edit"
                        data-title="{{ json_encode($tag->toArray()['name']) }}"
                        data-slug="{{ $tag->slug }}"
                        data-route="{{ route('dashboard.tag.update',['tag' => $tag->id]) }}"
                ><i class="feather icon-edit"></i></span>
                <span class="text-danger" onclick="js_delete('{{route('dashboard.tag.destroy',['tag' => $tag->id])}}')"
                ><i class="feather icon-trash"></i></span>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>

      <!-- add new sidebar starts -->
      <div class="add-new-data-sidebar">
          <div class="overlay-bg"></div>
          <div class="add-new-data">
              <form class="was-validated" id="js__form-edit" method="POST">
                  <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                      <div>
                          <h4 class="text-uppercase" id="js__modal-title">...</h4>
                      </div>
                      <div class="hide-data-sidebar">
                          <i class="feather icon-x"></i>
                      </div>
                  </div>
                  <div class="data-items pb-3">
                      <div class="data-fields px-2 mt-3">
                              @csrf
                              <div id="updating"></div>
                              @foreach($langs as $lang)
                                  <div class="mb-2">
                                      <fieldset class="form-label-group mt-1">
                                          <input
                                              type="text"
                                              class="form-control"
                                              id="edit_name_{{ $lang->alias }}"
                                              name="name[{{$lang->alias}}]"
                                              placeholder="{{ __('dashboard.tag.title').'('.$lang->alias.')' }}"
                                              required
                                          >
                                          <label for="edit_name_{{ $lang->alias }}">{{ __('dashboard.tag.title').' ('.$lang->alias.')' }} <i class="{{ $lang->icon }}"></i> </label>
                                      </fieldset>
                                  </div>
                              @endforeach
                          <div class="mb-2">
                              <fieldset class="form-label-group mt-1">
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="edit_slug"
                                      name="slug"
                                      pattern="[a-z0-9\-]+"
                                      maxlength="200"
                                      placeholder="{{ __('dashboard.tag.slug') }}"
                                      required
                                  >
                                  <label for="edit_slug">{{ __('dashboard.tag.slug') }} <i class="feather icon-external-link"></i></label>
                              </fieldset>
                          </div>

                      </div>
                  </div>
                  <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                      <div class="add-data-btn">
                          <button type="submit" class="btn btn-primary" id="js__modal-submit-btn">{{ __('actions.title') }}</button>
                      </div>
                      <div class="cancel-data-btn">
                          <button class="btn btn-outline-danger">{{ __('actions.delete') }}</button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
      <!-- add new sidebar ends -->
    {{-- DataTable ends --}}
  </section>
  {{-- Data list view end --}}
@endsection
@section('vendor-script')
  {{-- vendor js files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
  <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection
@section('page-script')

  <script>

      // Close sidebar
      $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
          $(".add-new-data").removeClass("show")
          $(".overlay-bg").removeClass("show")
          $("#data-name, #data-price").val("")
          $("#data-category, #data-status").prop("selectedIndex", 0)
      })

      // On Edit
      $('.js__edit').on("click",function(e){
          e.stopPropagation();
          $('#js__modal-title').text('{{ __('dashboard.tag.edit') }}')
          $('#js__modal-submit-btn').text('{{ __('dashboard.tag.update') }}')
          let put_method = '@method('PUT')'
          $('#updating').html(put_method)
          let route = $(this).data('route')
          let title = $(this).data('title')
          let slug = $(this).data('slug')
          $('#edit_name_ru').val(title.ru);
          $('#edit_name_uz').val(title.uz);
          $('#edit_name_en').val(title.en);
          $('#edit_slug').val(slug);
          $('#js__form-edit').attr('action',route)
          // $('#data-name').val('Altec Lansing - Bluetooth Speaker');
          // $('#data-price').val('$99');
          $(".add-new-data").addClass("show");
          $(".overlay-bg").addClass("show");
      });
      // On Add
      $('.js__add').on("click",function(e){
          e.stopPropagation();
          $('#js__modal-title').text('{{ __('dashboard.tag.add') }}')
          $('#js__modal-submit-btn').text('{{ __('dashboard.tag.add') }}')
          $('#updating > input').remove()
          let route = '{{ route('dashboard.tag.store') }}'
          $('#edit_name_ru').val('');
          $('#edit_name_uz').val('');
          $('#edit_name_en').val('');
          $('#js__form-edit').attr('action',route)
          $(".add-new-data").addClass("show");
          $(".overlay-bg").addClass("show");
      });


    // init list view datatable
    var dataListView = $(".data-list-view").DataTable({
      responsive: false,
      columnDefs: [
        {
          orderable: true,
          targets: 0,
          checkboxes: { selectRow: true }
        }
      ],
      dom:
        '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
      oLanguage: {
        sLengthMenu: "_MENU_",
        sSearch: ""
      },
      aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
      select: {
        style: "multi"
      },
      order: [[1, "asc"]],
      bInfo: false,
      pageLength: 10,
      buttons: [],
      initComplete: function(settings, json) {
        $(".dt-buttons .btn").removeClass("btn-secondary")
      }
    });

    dataListView.on('draw.dt', function(){
      setTimeout(function(){
        if (navigator.userAgent.indexOf("Mac OS X") != -1) {
          $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
        }
      }, 50);
    });
    // To append actions dropdown before add new button
    var actionDropdown = $(".actions-dropodown")
    actionDropdown.insertBefore($(".top .actions .dt-buttons"))
    // Scrollbar
    if ($(".data-items").length > 0) {
      new PerfectScrollbar(".data-items", { wheelPropagation: false })
    }
    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
      $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }

    function js_delete(route){
        Swal.fire({
            title: '{{ __('message.are_you_sure') }}',
            text: '{{ __('message.dont_revert') }}',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '{{ __('message.yes_delete') }}',
            cancelButtonText: '{{ __('message.cancel') }}',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                axios.delete(route).then(response => {
                    let data = response.data
                    if (data.status === 'success'){
                        Swal.fire({
                            title: data.message.title,
                            text: data.message.text,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok',
                            customClass: {
                                confirmButton: 'btn btn-primary',
                            },
                            buttonsStyling: false
                        }).then(function (result){
                            if (result.value){
                                window.location.reload()
                            }else{

                            }
                        });
                    }
                }).catch(response => {
                    Swal.fire({
                        type: 'error',
                        title: '{{ __('message.deleted') }}',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                })

            }
        });
    }

  </script>
@endsection
