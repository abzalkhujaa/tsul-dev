@extends('dashboard.layouts.dashboard')

@section('title', $user->username)

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
{{--    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">--}}
{{--    <style>--}}

{{--        @media (max-width: 576px) {--}}
{{--            .modal-slide-in.show .modal-dialog,--}}
{{--            .modal-slide-in .modal.show .modal-dialog{--}}
{{--                width: 100%!important;--}}
{{--            }--}}
{{--        }--}}

{{--        @media (min-width: 576px) and (max-width: 768px) {--}}
{{--            .modal-slide-in.show .modal-dialog,--}}
{{--            .modal-slide-in .modal.show .modal-dialog{--}}
{{--                width: 100%!important;--}}
{{--            }--}}
{{--        }--}}

{{--        .modal-backdrop{--}}
{{--            background-color: #ffffff!important;--}}
{{--        }--}}
{{--        .modal-backdrop.show{--}}
{{--            opacity: 0.8!important;--}}
{{--        }--}}
{{--    </style>--}}
    <button onclick="testFunc('{{ route('dashboard.staff.profile.index',['user' => $user->username]) }}')"></button>
@endsection

@section('content')
    <section id="user-pagination">
        @include('components.panel.user-pagination',['user' => $user, 'active' => 'main'])
    </section>
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>
        function testFunc(url){
            let title = $('title').val()
        }

    </script>


    {{-- Page js files --}}
    {{--    <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>--}}
@endsection
