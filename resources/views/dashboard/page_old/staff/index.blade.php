@extends('dashboard.layouts.dashboard')

@section('title', __('dashboard.staffs'))

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
    <style>

        @media (max-width: 576px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }

        @media (min-width: 576px) and (max-width: 768px) {
            .modal-slide-in.show .modal-dialog,
            .modal-slide-in .modal.show .modal-dialog{
                width: 100%!important;
            }
        }

        .modal-backdrop{
            background-color: #ffffff!important;
        }
        .modal-backdrop.show{
            opacity: 0.8!important;
        }
    </style>
@endsection

@section('content')
{{--    {{ dump($errors) }}--}}
    <!-- users list start -->
    <section class="app-user-list">
        <!-- users filter start -->
        <div class="card">
            <h5 class="card-header">Фильтр поиска</h5>
            <div class="d-flex justify-content-between align-items-center mx-50 row pt-0 pb-2">
                <div class="col-md-4 user_role"></div>
                <div class="col-md-4 user_plan"></div>
                <div class="col-md-4 user_status"></div>
            </div>
        </div>
        <!-- users filter end -->
        <!-- list section start -->
        <div class="card">
            <div class="card-header">
                <button class="btn add-new btn-primary mt-50" tabindex="0" type="button" data-toggle="modal" data-target="#modals-slide-in">
                    <i class="feather icon-user-plus mr-1"></i> <span>{{ __('page.dashboard.staff.create') }}</span>
                </button>
            </div>
            <div class="card-body">
                <div class="card-content">
                    lorem
                </div>
            </div>
            <div class="card-datatable table-responsive pt-0">

            </div>
            <!-- Modal to add new user starts-->
            <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
                <div class="d-flex align-items-center justify-content-center h-100 w-75">
                    <img src="{{asset('images/pages/login-v2.svg')}}" class="img-fluid" alt="">
                </div>
                <div class="modal-dialog">
                    <form class="add-new-user modal-content pt-0 was-validated" method="POST" action="{{ route('dashboard.staff.store') }}">
                        @csrf
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                        <div class="modal-header mb-1">
                            <h5 class="modal-title" id="user_create">{{ __('dashboard.new_user')}}</h5>
                        </div>
                        <div class="modal-body flex-grow-1">
                            <div class="mb-2">
                                <x-html.input name="username" :label="__('dashboard.username')" type="text" pattern="{{ 'pattern=^[a-z0-9_]{1,30}$' }}" :disabled="false" :required="true" />
                                <small class="form-text text-muted">Вы можете использовать строчные латинские буквы, цифры и подчеркивание. </small>
                            </div>
                            <div class="mb-2">
                                <x-html.input name="email" :label="__('dashboard.email')" type="email" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="password" :label="__('dashboard.password')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="first_name" :label="__('dashboard.profile_field.fname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="last_name" :label="__('dashboard.profile_field.lname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="mb-2">
                                <x-html.input name="patronymic" :label="__('dashboard.profile_field.pname')" type="text" :disabled="false" :required="true" />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="user-role">Пол пользователя:</label>
                                <select id="user-gender" class="form-control" name="gender" required>
                                    <option value="N">{{ __('dashboard.no_gender') }}</option>
                                    <option value="M">{{ __('dashboard.male') }}</option>
                                    <option value="F">{{ __('dashboard.female') }}</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mr-1 data-submit">{{ __('page.dashboard.staff.create') }}</button>
                            <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('message.cancel') }}</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal to add new user Ends-->
        </div>
        <!-- list section end -->
    </section>
    <!-- users list ends -->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>--}}
@endsection
