@extends('dashboard.layouts.dashboard')

@section('title', __('page.dashboard.staff.table_title'))

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')
    <!-- users list start -->
    <section class="app-user-list">
        <!-- users filter start -->
        <div class="card">
            <h5 class="card-header">Фильтр поиска</h5>
            <div class="d-flex justify-content-between align-items-center mx-50 row pt-0 pb-2">
                <div class="col-md-4 user_role">
                </div>
                <div class="col-md-4 user_plan"></div>
                <div class="col-md-4 user_status"></div>
            </div>
        </div>
        <!-- users filter end -->
        <!-- list section start -->
        <div class="card">
            <div class="card-body">
                <div class="card-content">
                    <div class="card-datatable table-responsive pt-0">
                        <table class="user-list-table table">
                            <thead class="thead-light">
                            <tr>
                                <th></th>
                                <th><i class="feather icon-user"></i> {{ __('dashboard.staff') }}</th>
                                <th><i class="feather icon-at-sign"></i> {{ __('dashboard.email') }}</th>
                                <th>{{ __('dashboard.position') }}</th>
                                <th>{{ __('dashboard.role') }}</th>
                                <th>{{ __('dashboard.status') }}</th>
                                <th>{{ __('dashboard.action.title') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <td></td>
                            <td>
                                <div class="d-flex justify-content-left align-items-center">
                                    <div class="avatar-wrapper">
                                        <div class="avatar  mr-1">
                                            @if(auth()->user()->status->image)
                                            <img src="http://localhost:8000/images/avatars/2-small.png" alt="Avatar" height="32" width="32">
                                            @else
                                                <span class="avatar-content">{{ auth()->user()->first_name[0] . auth()->user()->last_name[0] }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <a href="http://localhost:8000/app/user/view" class="user_name text-truncate">
                                            <span class="font-weight-bold">{{ auth()->user()->full_name }}</span>
                                        </a>
                                        <small class="emp_post text-muted">{{ '@'.auth()->user()->username }}</small>
                                    </div>
                                </div>
                            </td>
                            <td>{{ auth()->user()->email }}</td>
                            <td>"dhsakjdd"</td>
                            <td>{!! auth()->user()->role_icon() !!} {{ auth()->user()->roles->first()->name }}</td>
                            <td>{{ auth()->user()->status->type }}</td>
                            <td><i class="feather icon-list"></i></td>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Modal to add new user starts-->
            <div class="modal modal-slide-in new-user-modal fade" id="modals-slide-in">
                <div class="modal-dialog">
                    <form class="add-new-user modal-content pt-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                        <div class="modal-header mb-1">
                            <h5 class="modal-title" id="exampleModalLabel">New User</h5>
                        </div>
                        <div class="modal-body flex-grow-1">
                            <div class="form-group">
                                <label class="form-label" for="basic-icon-default-fullname">Full Name</label>
                                <input
                                        type="text"
                                        class="form-control dt-full-name"
                                        id="basic-icon-default-fullname"
                                        placeholder="John Doe"
                                        name="user-fullname"
                                        aria-label="John Doe"
                                        aria-describedby="basic-icon-default-fullname2"
                                />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-icon-default-uname">Username</label>
                                <input
                                        type="text"
                                        id="basic-icon-default-uname"
                                        class="form-control dt-uname"
                                        placeholder="Web Developer"
                                        aria-label="jdoe1"
                                        aria-describedby="basic-icon-default-uname2"
                                        name="user-name"
                                />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-icon-default-email">Email</label>
                                <input
                                        type="text"
                                        id="basic-icon-default-email"
                                        class="form-control dt-email"
                                        placeholder="john.doe@example.com"
                                        aria-label="john.doe@example.com"
                                        aria-describedby="basic-icon-default-email2"
                                        name="user-email"
                                />
                                <small class="form-text text-muted"> You can use letters, numbers & periods </small>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="user-role">User Role</label>
                                <select id="user-role" class="form-control">
                                    <option value="subscriber">Subscriber</option>
                                    <option value="editor">Editor</option>
                                    <option value="maintainer">Maintainer</option>
                                    <option value="author">Author</option>
                                    <option value="admin">Admin</option>
                                </select>
                            </div>
                            <div class="form-group mb-2">
                                <label class="form-label" for="user-plan">Select Plan</label>
                                <select id="user-plan" class="form-control">
                                    <option value="basic">Basic</option>
                                    <option value="enterprise">Enterprise</option>
                                    <option value="company">Company</option>
                                    <option value="team">Team</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mr-1 data-submit">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal to add new user Ends-->
        </div>
        <!-- list section end -->
    </section>
    <!-- users list ends -->
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
{{--    <script src="{{ asset(mix('js/scripts/pages/app-user-list.js')) }}"></script>--}}

    <script>
        let dt_responsive_table = $('.user-list-table')
        let dt_responsive = dt_responsive_table.DataTable({
            pageLength: 10,
            aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            columnDefs: [
                {
                    className: 'control',
                    orderable: false,
                    targets: 0
                },
            ],
            buttons: [
                {
                    text: '<i class="feather icon-database"></i> Excel',
                    className: 'btn btn-success mt-50',
                    attr: {

                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                },
                {
                    text: '<i class="feather icon-user-plus"></i> Add New User',
                    className: 'add-new btn btn-primary mt-50',
                    attr: {
                        'data-toggle': 'modal',
                        'data-target': '#modals-slide-in'
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            dom:
                '<"d-flex justify-content-between align-items-center header-actions mx-1 row mt-75"' +
                '<"col-lg-12 col-xl-6" l>' +
                '<"col-lg-12 col-xl-6 pl-xl-75 pl-0"<"dt-action-buttons text-xl-right text-lg-left text-md-right text-left d-flex align-items-center justify-content-lg-end align-items-center flex-sm-nowrap flex-wrap mr-1"<"mr-1"f>B>>' +
                '>t' +
                '<"d-flex justify-content-between mx-2 row mb-1"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            },
            language: {
                "decimal":        "",
                "emptyTable":     "Нет данных в таблице",
                "info":           "Показано с _START_ по _END_ из _TOTAL_ записей",
                "infoEmpty":      "Показано с 0 по 0 из 0 записей",
                "infoFiltered":   "(отфильтровано из _MAX_ записей)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Показать _MENU_ записей",
                "loadingRecords": "Загрузка...",
                "processing":     "Обработка...",
                "search":         "Поиск:",
                "zeroRecords":    "Сотрудник не найден",
                "paginate": {
                    "first":      "Первый",
                    "last":       "Последний",
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });

    </script>
@endsection
