@extends('dashboard.layouts.dashboard')

@section('title',__('dashboard.post.create'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection


@section('content')
    {{--    {{ Request::old() }}--}}
    <form>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="card-content">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="card-content">
                            <div class="">
                                <select data-placeholder="Select a state..." name="lang" class="select2-icons form-control" id="select2-icons">
                                    <optgroup label="Language">
                                        @foreach($langs as $lang)
                                            <option value="{{ $lang->alias }}" data-icon="user">
                                                {{ $lang->getTranslation('title',app()->getLocale()) }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>

                            <hr>
                            <label>Choose tags:</label>
                            <select class="select2 form-control" name="tags" multiple>
                                <optgroup label="Tags">
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->alias }}">{{ $tag->getTranslation('name',app()->getLocale()) }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal modal-slide-in fade" id="js__create">
        <div class="modal-dialog sidebar-sm">
            <form method="POST" action="{{ route('dashboard.tag.store') }}" class="was-validated add-new-record modal-content pt-0">
                @csrf
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="modal-header mb-1">
                    <h5 class="modal-title" id="create_modal-label">{{ __('dashboard.tag.create') }}</h5>
                </div>
                <div class="modal-body flex-grow-1">
                    @foreach($langs as $lang)
                        <div class="mb-2">
                            <fieldset class="form-label-group mt-1">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="name_{{ $lang->alias }}"
                                    name="name[{{$lang->alias}}]"
                                    placeholder="{{ __('dashboard.tag.title').'('.$lang->alias.')' }}"
                                    required
                                    value="{{ Request::old('name["'.$lang->alias.'"]') }}"
                                >
                                <label for="name_{{ $lang->alias }}">{{ __('dashboard.tag.title').'('.$lang->alias.')' }} <i class="{{ $lang->icon }}"></i> </label>
                            </fieldset>
                        </div>
                    @endforeach
                    <button type="submit" class="btn btn-primary data-submit mr-1">{{ __('actions.add') }}</button>
                    <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">{{ __('actions.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset('vendor/axios/axios.min.js') }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/scripts/forms/form-select2.js') }}"></script>
    {{-- Page js files --}}
    <script>
        let selectIcons = $('select2-icons');
        selectIcons.each(function () {
            let $this = $(this);
            $this.wrap('<div class="position-relative"></div>');
            $this.select2({
                dropdownAutoWidth: true,
                width: '100%',
                minimumResultsForSearch: Infinity,
                dropdownParent: $this.parent(),
                templateResult: iconFormat,
                templateSelection: iconFormat,
                escapeMarkup: function (es) {
                    return es;
                }
            });
        });

        // Format icon
        function iconFormat(icon) {
            let originalOption = icon.element;
            if (!icon.id) {
                return icon.text;
            }

            return feather.icons[$(icon.element).data('icon')].toSvg() + icon.text;
        }
        function js_delete(route){
            Swal.fire({
                title: '{{ __('message.are_you_sure') }}',
                text: '{{ __('message.dont_revert') }}',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '{{ __('message.yes_delete') }}',
                cancelButtonText: '{{ __('message.cancel') }}',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    axios.delete(route).then(response => {
                        let data = response.data
                        if (data.status === 'success'){
                            Swal.fire({
                                title: data.message.title,
                                text: data.message.text,
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok',
                                customClass: {
                                    confirmButton: 'btn btn-primary',
                                },
                                buttonsStyling: false
                            }).then(function (result){
                                if (result.value){
                                    window.location.reload()
                                }else{

                                }
                            });
                        }
                    }).catch(response => {
                        Swal.fire({
                            icon: 'error',
                            title: '{{ __('message.deleted') }}',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    })

                }
            });
        }


    </script>
@endsection




