@extends('dashboard.layouts.dashboard')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <fieldset class="form-label-group mt-1">
                    <input
                            type="text"
                            class="form-control"
                            id="test"
                            name="test"
                            placeholder="Test test"
                            value="{{ old('test') }}"
                    >
                    <label for="test">Test TEST</label>
                    @error('test')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </fieldset>

            </div>
        </div>
    </div>
@endsection
