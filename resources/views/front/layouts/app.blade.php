<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('front/css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <div class="wrapper">
        @include('front.layouts.panel.header')

        @yield('content')

        @include('front.layouts.panel.footer')
    </div>

</div>

<script src="{{ asset('front/js/app.js') }}"></script>
<script src="{{ asset('front/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/assets/js/fancybox.min.js') }}"></script>
<script src="{{ asset('front/js/main.js') }}"></script>

</body>
</html>
