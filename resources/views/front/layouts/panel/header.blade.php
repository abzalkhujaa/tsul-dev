<section class="block_zero">
    <a href="#" class="logo">
        <img src="{{ asset('front/img/gerb_mini.png') }}" alt="" />
        <span>Ташкентский Государственный <i>Юридический Университет</i></span>
    </a>
    <div class="container h_width">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <ul class="menu_list">
                    <li><a href="{{ route('front.news.index') }}">Новости</a></li>
                    <li><a href="#">Библиотека</a></li>
                    <li><a href="#">Наука</a></li>
                </ul>
            </div>
            <div class="col-lg-4"></div>
            <div class="col-lg-4 col-md-12">
                <ul class="menu_list">
                    <li><a href="http:">Онлайн платформа</a></li>
                    <li><a href="#">Кабинет</a></li>
                    <li>
                        <ul class="lang">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ру <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Ўз</a></li>
                                    <li><a href="#">O`z</a></li>
                                </ul>
                            </li>
{{--                            <li class="specialBoxList">--}}
{{--                                <div class="special_box">--}}
{{--                                    <div class="icon_accessibility" role="" data-toggle="dropdown" data-placement="bottom" aria-expanded="true">--}}
{{--                                        <img src="{{ asset('front/img/icon_eyes.png') }}" alt="" />--}}
{{--                                    </div>--}}
{{--                                    <div class="dropdown-menu dropdown-menu-right specialViewArea no-propagation">--}}
{{--                                        <div class="triangle2"></div>--}}

{{--                                        <div class="appearance">--}}
{{--                                            <p class="specialTitle">Кўриниш</p>--}}

{{--                                            <div class="squareAppearances">--}}
{{--                                                <div class="squareBox spcNormal" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Оддий кўриниш">A</div>--}}
{{--                                            </div>--}}
{{--                                            <div class="squareAppearances">--}}
{{--                                                <div class="squareBox spcWhiteAndBlack" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Оқ-қора кўриниш">A</div>--}}
{{--                                            </div>--}}
{{--                                            <div class="squareAppearances">--}}
{{--                                                <div class="squareBox spcDark" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Қоронғилашган кўриниш">A</div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="appearance">--}}
{{--                                            <p class="specialTitle">Шрифт ўлчами</p>--}}
{{--                                            <div class="block blocked">--}}
{{--                                                <div class="sliderText"><span class="range">0</span>% га катталаштириш</div>--}}
{{--                                                <div id="fontSizer" class="defaultSlider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div><div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div></div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="block_first">
    <div class="container h_width">
        <div class="row">
            <div class="col-lg-4">
                <ul class="white_bg_menu_left">
                    <li><a href="#">Об университете</a></li>
                    <li><a href="#">Поступающим</a></li>
                    <li><a href="#">Студентам</a></li>
                </ul>
            </div>
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <ul class="white_bg_menu_right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Факультеты и центры</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Link 1</a>
                            <a class="dropdown-item" href="#">Link 2</a>
                            <a class="dropdown-item" href="#">Link 3</a>
                        </div>
                    </li>
                    <li><a href="#">Международное сотрудничество</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
