<section class="block_eighth">
    <div class="container h_width">
        <div class="row footer">
            <div class="col-lg-4">
                <div class="footer_logo">
                    <img src="{{ asset('front/img/gerb_uzb.png') }}" alt="" />
                    <span>Ташкентский Государственный <i>Юридический Университет</i></span>
                </div>
                <ul class="share_list">
                    <li><img src="{{ asset('front/img/telephone.png') }}" alt="" />+71 777 77 77</li>
                    <li><img src="{{ asset('front/img/message.png') }}" alt="" />tsul.uzwebline @</li>
                </ul>
            </div>
            <div class="col-lg-8">
                <ul class="footer_menu">
                    <li><a href="#">Об университете</a></li>
                    <li><a href="#">Факультеты магистратуры</a></li>
                    <li><a href="#">Факультеты и центры</a></li>
                    <li><a href="#">Поступающим</a></li>
                    <li><a href="#">Виртуальный тур по университету</a></li>
                    <li><a href="#">Международное сотрудничество</a></li>
                    <li><a href="#">Студентам</a></li>
                    <li><a href="#">Брошюра для студентов</a></li>
                    <li><a href="#">Вакансии</a></li>
                    <li><a href="#">Докторантура</a></li>
                    <li><a href="#">Факультеты бакалавриата</a></li>
                </ul>
                <ul class="last_list">
                    <li>© 2020-2021 все права защищены</li>
                    <li>Разработка: <a href="#"><img src="{{ asset('front/img/uzinfocom.png') }}" alt="" /></a></li>
                </ul>
            </div>

        </div>
    </div>
</section>
<section class="block_ninth">
    <div class="container h_width">
        <div class="row">
            <div class="col-lg-4">
                <ul class="share_list footer_share_list">
                    <li>
                        <a href="#"><img src="{{ asset('front/img/facebook.png') }}" alt="" /></a>
                        <a href="#"><img src="{{ asset('front/img/twitter.png') }}" alt="" /></a>
                        <a href="#"><img src="{{ asset('front/img/g_plus.png') }}" alt="" /></a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-8">
                <p class="attention_text">Внимание! Если Вы нашли ошибку в тексте, выделите её и нажмите Ctrl+Enter для уведомления администрации</p>
            </div>
        </div>
    </div>
</section>
