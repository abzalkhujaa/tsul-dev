@extends('front.layouts.app')


@section('content')
    <section class="block_second">
        <div class="container h_width">
            <div class="news">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="bread_crumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Главная страница</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Новости</li>
                                </ol>
                            </nav>
                        </div>
                        <span class="inner_title">Новости</span>
                        <div class="news_type_box">
                            @foreach($tags as $tag)
                                <a href="{{ route('front.news.tag.index',['tag' => $tag->id]) }}" class="news_type">{{ $tag->getTranslation('name',app()->getLocale()) }}</a>
                            @endforeach
                        </div>
                        <x-news-item :post="$news[0]"/>
                    </div>
                    <div class="col-lg-6 right_side_b">
                        <x-social-stat/>
{{--=========================================== TOP 4 NEWS========================================================--}}
                        <x-news-item :post="$news[1]"/>
                        <x-news-item :post="$news[2]"/>
                        <x-news-item :post="$news[3]"/>
                        <x-news-item :post="$news[4]"/>
{{--========================================== TOP 4 NEWS=========================================================--}}
                    </div>
                    <div class="col-lg-6 left_side_b">
                        <x-news-item :post="$news[5]"/>
                        <x-news-item :post="$news[6]"/>
                        <x-news-item :post="$news[7]"/>
                        <x-news-item :post="$news[8]"/>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <span class="mini_title">Анонсы</span>
                                <ul class="anons_list">
                                    @foreach($anons as $one)
                                        <li>
                                            <p><span>{{ $one->created_at->format('d') }}<i>{{ $one->created_at->format('M') }}</i></span></p>
                                            <a
                                                href="{{ route('front.news.show',['post' => $one->id]) }}"
                                                title="{{ $one->getTranslation('title',app()->getLocale()) }}">
                                                {{ Str::limit($one->getTranslation('title',app()->getLocale()),55) }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <span class="mini_title">Последние статьи</span>
                                <ul class="recent_articles">

                                    @foreach($article as $one)
                                        <li>
                                            <span class="round_img"><img src="{{ asset($one->image) }}" alt="" /></span>
                                            <a href="{{ route('front.news.show',['post' => $one->id]) }}">{{ $one->author->first_name . ' ' . $one->author->last_name }}</a>
                                            <span class="rec_art_tex">
                                                {{ Str::limit($one->getTranslation('title',app()->getLocale())) }}
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('front.news.all') }}" class="in_all_news_link">Все новости</a>
                </div>
            </div>
        </div>
    </section>
@endsection

