@extends('front.layouts.app')

@section('content')
    <section class="block_second">
        <div class="container h_width">
            <div class="news">
                <div class="row">
                    <div class="col-lg-8 page_2_left">
                        <div class="bread_crumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Главная страница</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Новости</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="news_type_box">
                            @foreach($post->tags as $tag)
                                <a href="{{ route('front.news.tag.index',['tag' => $tag->id,'alias' => $tag->getTranslation('slug',app()->getLocale())]) }}" class="news_type">{{ $tag->getTranslation('name',app()->getLocale()) }}</a>
                            @endforeach
                        </div>
                        <div class="date_view_count">
                            <span class="date">06.01.2021</span>
                            <span class="view_count"><img src="{{ asset('front/img/view_icon.png') }}" alt="" />2348</span>
                        </div>
                        <span class="news_in_title">
                            {{ $post->getTranslation('title',app()->getLocale()) }}
                        </span>

                           @if(!is_null($post->image))
                                <div class="my-3">
                                    <img src="{{ asset($post->image) }}" class="w-100" alt="#">
                                </div>
                            @endif

                        <div class="page_2_left_full_text">
                            {!! $post->getTranslation('content',app()->getLocale()) !!}
                        </div>

                        <x-widget.last-news/>

                    </div>
                    <div class="col-lg-4 page_2_right">
                        <x-widget.social-stat/>
                        <x-widget.last-announcement/>
                        <x-widget.last-article/>
                        <x-widget.youtube/>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
