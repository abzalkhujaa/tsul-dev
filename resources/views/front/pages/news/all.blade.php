@extends('front.layouts.app')


@section('content')
    <section class="block_second">
        <div class="container h_width">
            <div class="news">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="bread_crumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Главная страница</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Новости</li>
                                </ol>
                            </nav>
                        </div>
                        <span class="inner_title">Новости</span>
                        <div class="news_type_box">
                            @foreach($tags as $tag)
                                <a href="{{ route('front.news.tag.index',['tag' => $tag->id]) }}" class="news_type">{{ $tag->getTranslation('name',app()->getLocale()) }}</a>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-lg-6 right_side_b">
                        <ul class="social_stat">
                            <li>
                                <span class="mini_title">Последние статьи</span>
                            </li>
                            <li>
                                <span><img src="{{ asset('front/img/telegram_icon.png') }}" alt="" /></span>
                                <p>21,290</p>
                                <i>подписчиков</i>
                            </li>
                            <li>
                                <span><img src="{{ asset('front/img/facebook_icon.png') }}" alt="" /></span>
                                <p>21,290</p>
                                <i>читателей</i>
                            </li>
                            <li>
                                <span><img src="{{ asset('front/img/twitter_icon.png') }}" alt="" /></span>
                                <p>21,290</p>
                                <i>подписчиков</i>
                            </li>
                            <li>
                                <span><img src="{{ asset('front/img/youtube_icon.png') }}" alt="" /></span>
                                <p>21,290</p>
                                <i>подписчиков</i>
                            </li>
                        </ul>
                    </div>
                    @foreach($posts as $post)
                        <div class="col-md-3 col-sm-6 col-12">
                            <x-news-item :post="$post"/>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

