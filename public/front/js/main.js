/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/front/js/main.js":
/*!************************************!*\
  !*** ./resources/front/js/main.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$('[data-fancybox=\"images\"]').fancybox();\n$(\"#actual_news_slider\").owlCarousel({\n  loop: true,\n  autoplay: true,\n  autoplayTimeout: 7000,\n  nav: true,\n  animateOut: 'fadeOut',\n  responsive: {\n    0: {\n      items: 1\n    },\n    600: {\n      items: 1\n    },\n    1000: {\n      items: 1\n    }\n  },\n  navigation: true,\n  pagination: true\n});\n$(\"#links_slider\").owlCarousel({\n  loop: true,\n  autoplay: true,\n  autoplayTimeout: 7000,\n  smartSpeed: 1000,\n  nav: true,\n  responsive: {\n    0: {\n      items: 1\n    },\n    600: {\n      items: 3\n    },\n    1000: {\n      items: 5\n    }\n  },\n  navigation: true,\n  pagination: false\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvZnJvbnQvanMvbWFpbi5qcz9jYjEzIl0sIm5hbWVzIjpbIiQiLCJmYW5jeWJveCIsIm93bENhcm91c2VsIiwibG9vcCIsImF1dG9wbGF5IiwiYXV0b3BsYXlUaW1lb3V0IiwibmF2IiwiYW5pbWF0ZU91dCIsInJlc3BvbnNpdmUiLCJpdGVtcyIsIm5hdmlnYXRpb24iLCJwYWdpbmF0aW9uIiwic21hcnRTcGVlZCJdLCJtYXBwaW5ncyI6IkFBQUFBLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCQyxRQUE5QjtBQUVBRCxDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QkUsV0FBekIsQ0FBcUM7QUFDakNDLE1BQUksRUFBRSxJQUQyQjtBQUVqQ0MsVUFBUSxFQUFFLElBRnVCO0FBR2pDQyxpQkFBZSxFQUFFLElBSGdCO0FBSWpDQyxLQUFHLEVBQUUsSUFKNEI7QUFLakNDLFlBQVUsRUFBRSxTQUxxQjtBQU1qQ0MsWUFBVSxFQUFFO0FBQ1IsT0FBRztBQUNDQyxXQUFLLEVBQUU7QUFEUixLQURLO0FBSVIsU0FBSztBQUNEQSxXQUFLLEVBQUU7QUFETixLQUpHO0FBT1IsVUFBTTtBQUNGQSxXQUFLLEVBQUU7QUFETDtBQVBFLEdBTnFCO0FBaUJqQ0MsWUFBVSxFQUFFLElBakJxQjtBQWtCakNDLFlBQVUsRUFBRTtBQWxCcUIsQ0FBckM7QUFxQkFYLENBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJFLFdBQW5CLENBQStCO0FBQzNCQyxNQUFJLEVBQUUsSUFEcUI7QUFFM0JDLFVBQVEsRUFBRSxJQUZpQjtBQUczQkMsaUJBQWUsRUFBQyxJQUhXO0FBSTNCTyxZQUFVLEVBQUMsSUFKZ0I7QUFLM0JOLEtBQUcsRUFBQyxJQUx1QjtBQU0zQkUsWUFBVSxFQUFDO0FBQ1AsT0FBRTtBQUNFQyxXQUFLLEVBQUM7QUFEUixLQURLO0FBSVAsU0FBSTtBQUNBQSxXQUFLLEVBQUM7QUFETixLQUpHO0FBT1AsVUFBSztBQUNEQSxXQUFLLEVBQUM7QUFETDtBQVBFLEdBTmdCO0FBaUIzQkMsWUFBVSxFQUFHLElBakJjO0FBa0IzQkMsWUFBVSxFQUFFO0FBbEJlLENBQS9CIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2Zyb250L2pzL21haW4uanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKCdbZGF0YS1mYW5jeWJveD1cImltYWdlc1wiXScpLmZhbmN5Ym94KCk7XG5cbiQoXCIjYWN0dWFsX25ld3Nfc2xpZGVyXCIpLm93bENhcm91c2VsKHtcbiAgICBsb29wOiB0cnVlLFxuICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgIGF1dG9wbGF5VGltZW91dDogNzAwMCxcbiAgICBuYXY6IHRydWUsXG4gICAgYW5pbWF0ZU91dDogJ2ZhZGVPdXQnLFxuICAgIHJlc3BvbnNpdmU6IHtcbiAgICAgICAgMDoge1xuICAgICAgICAgICAgaXRlbXM6IDFcbiAgICAgICAgfSxcbiAgICAgICAgNjAwOiB7XG4gICAgICAgICAgICBpdGVtczogMVxuICAgICAgICB9LFxuICAgICAgICAxMDAwOiB7XG4gICAgICAgICAgICBpdGVtczogMVxuICAgICAgICB9XG4gICAgfSxcbiAgICBuYXZpZ2F0aW9uOiB0cnVlLFxuICAgIHBhZ2luYXRpb246IHRydWVcbn0pO1xuXG4kKFwiI2xpbmtzX3NsaWRlclwiKS5vd2xDYXJvdXNlbCh7XG4gICAgbG9vcDogdHJ1ZSxcbiAgICBhdXRvcGxheTogdHJ1ZSxcbiAgICBhdXRvcGxheVRpbWVvdXQ6NzAwMCxcbiAgICBzbWFydFNwZWVkOjEwMDAsXG4gICAgbmF2OnRydWUsXG4gICAgcmVzcG9uc2l2ZTp7XG4gICAgICAgIDA6e1xuICAgICAgICAgICAgaXRlbXM6MVxuICAgICAgICB9LFxuICAgICAgICA2MDA6e1xuICAgICAgICAgICAgaXRlbXM6M1xuICAgICAgICB9LFxuICAgICAgICAxMDAwOntcbiAgICAgICAgICAgIGl0ZW1zOjVcbiAgICAgICAgfVxuICAgIH0sXG4gICAgbmF2aWdhdGlvbiA6IHRydWUsXG4gICAgcGFnaW5hdGlvbjogZmFsc2Vcbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/front/js/main.js\n");

/***/ }),

/***/ 3:
/*!******************************************!*\
  !*** multi ./resources/front/js/main.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\OpenServer\domains\tsulv2.loc\resources\front\js\main.js */"./resources/front/js/main.js");


/***/ })

/******/ });