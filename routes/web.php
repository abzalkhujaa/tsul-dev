<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;
use Spatie\DbDumper\Databases\PostgreSql;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});

Route::group([
    'namespace' => 'Front',
    'as' => 'front.',
], function () {

    Route::get('/', 'MainController@index')->name('index');
    Route::get('/news','PostController@index')->name('news.index');
    Route::get('/news/all','PostController@all')->name('news.all');
    Route::get('/news/tag/{tag}/{alias?}','PostController@tag')->name('news.tag.index');
    Route::get('/news/category/{category}/{alias?}','PostController@catagory')->name('news.category.index');
    Route::get('/news/{post}/{alias?}','PostController@show')->name('news.show');







//    Route::get('/post/{post}', function (Post $post) {
//        $oldLocale = session()->get('old_locale');
//        $locale = \App::getLocale();
//        $post = Post::where('alias->'.$locale,$post)->get();
//
//        if ($post === null && $oldLocale !== null && $oldLocale !== $locale) {
//            $post = Post::where('alias->'.$oldLocale,$post)->get();
//            if ($post === null) abort(404);
//            return redirect()->route('front.post.show',['post' => $post->alias]);
//        }
//
//        $langs = \App\Models\Language::all();
//
//        foreach ($langs as $lang){
//            $one = Post::where('alias->'.$lang->alias,$post)->get();
//
//            if ($one === null) abort(404);
//
//            return redirect()->route('front.post.show',['post' => $post->alias]);
//
//        }
//
//
//        Post::query()->whereRaw('(alias->\'en\')::jsonb ? \'dsadadad\'')->get()->dd();
//
//
//    })->name('post.show');




//    Route::get('post/create/{current_lang}','PostController@create')->name('post.create');

});

Auth::routes();

Route::group([
    'prefix' => 'dashboard',
    'namespace' => 'Dashboard',
    'as' => 'dashboard.',
], function () {
    Route::get('/', function () {})->name('index');
    Route::resource('tag', 'TagController')->except(['show', 'create', 'edit']);
    Route::resource('post', 'PostController')->except(['show']);
    Route::resource('event', 'EventController')->except(['show']);
    Route::resource('page', 'PageController')->except(['show']);
//    Route::get('post/create/{current_lang}','PostController@create')->name('post.create');

    Route::post('/file-upload', 'FileUploadController@upload')->name('file-upload');
    Route::delete('/file-delete', 'FileUploadController@destroy')->name('file-delete');
    Route::get('/file-get/{file}', 'FileUploadController@show')->name('file-get');


});
