<?php

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('posts',function (){
    return response()->json(Post::query()->latest()->get());
});
Route::get('page',function (){
    return response()->json(\App\Models\Page::all());
});
Route::get('page-type',function (){
    return response()->json(\App\Models\PageType::all());
});

Route::get('front-page',function (){
    $news = Category::whereDescription(Category::NEWS)->first()->posts->take(9)->toArray() ?? [];
    $article = Category::whereDescription(Category::ARTICLE)->first()->posts->take(5)->toArray() ?? [];
    $anons = Category::whereDescription(Category::ANNOUNCEMENT)->first()->posts->take(5)->toArray() ?? [];

    return response()->json(compact('news','article','anons'));
});

Route::get('tags',function (){
   return response()->json(Tag::all());
});

Route::get('events',function (){
   return response()->json([
       'fields' => \App\Models\Event::fields()
   ]);
});

Route::get('{lang}/tag/{alias}',function (string $lang,string $alias){
    try {
        $tag = (new Spatie\Tags\Tag)->where('slug->'.$lang,$alias)->first();
        $post = Post::withAnyTags([$tag])->get();
        return response()->json($post);
    }catch (Exception $exception){
        return response()->json([],404);
    }
});
