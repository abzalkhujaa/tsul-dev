<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c1_data = [
            'ru' => 'Новости',
            'uz' => 'Yangiliklar',
            'en' => 'News'
        ];

        $c1 = new Category();
        $c1->icon = 'fas fa-newspaper';
        $c1->setTranslations('title',$c1_data);
        $c1->alias = (string)\Str::of('News')->slug();
        $c1->save();

        $c2_data = [
            'ru' => 'Статья',
            'uz' => 'Maqola',
            'en' => 'Article'
        ];


        $c2 = new Category();
        $c2->icon = 'fas fa-address-card';
        $c2->setTranslations('title',$c2_data);
        $c2->alias = (string)\Str::of('Article')->slug();
        $c2->save();

        $c3_data = [
            'ru' => 'Анонс',
            'uz' => "E'lon",
            'en' => 'Announcement'
        ];

        $c3 = new Category();
        $c3->icon = 'fas fa-bell';
        $c3->setTranslations('title',$c3_data);
        $c3->alias = (string)\Str::of('Announcement')->slug();
        $c3->save();
    }
}
