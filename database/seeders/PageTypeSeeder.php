<?php

namespace Database\Seeders;

use App\Models\PageType;
use Illuminate\Database\Seeder;

class PageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $pt1 = PageType::create([
                'name' => 'video',
                'icon' => 'icon-film'
            ]);
            $pt2 = PageType::create([
                'name' => 'gallery',
                'icon' => 'icon-image'
            ]);
            $pt3 = PageType::create([
                'name' => 'simple',
                'icon' => 'icon-layout',
            ]);
            $pt3 = PageType::create([
                'name' => 'rector',
                'icon' => 'icon-star',
                'inputs' => json_encode([
                    'title',
                    'content',
                    'video',
                    'full_name',
                    'email',
                    'place',
                    'phone',
                    'telegram',
                    'facebook',
                    'twitter',
                    'biography'
                ])
            ]);

    }
}
