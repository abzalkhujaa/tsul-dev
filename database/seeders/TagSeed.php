<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'name' => [
                'en' => 'Education',
                'ru' => 'Образование',
                'uz' => "Ta'lim",
            ],
            'slug' => (string)\Str::of('Education')->slug()
        ]);
        Tag::create([
            'name' => [
                'en' => 'Grants',
                'ru' => 'Гранты',
                'uz' => "Grantlar",
            ],
            'slug' => (string)\Str::of('Grants')->slug()
        ]);
        Tag::create([
            'name' => [
                'en' => 'University',
                'ru' => 'Университет',
                'uz' => "Universitet",
            ],
            'slug' => (string)\Str::of('University')->slug()
        ]);
        Tag::create([
            'name' => [
                'en' => 'Student life',
                'ru' => 'Студенческая жизнь',
                'uz' => "Talabalik hayoti",
            ],
            'slug' => (string)\Str::of('Student life')->slug()
        ]);
        Tag::create([
            'name' => [
                'en' => 'The science',
                'ru' => 'Наука',
                'uz' => "Ilm-fan",
            ],
            'slug' => (string)\Str::of('The science')->slug()
        ]);
    }
}
