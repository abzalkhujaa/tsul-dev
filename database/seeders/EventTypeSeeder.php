<?php

namespace Database\Seeders;

use App\Models\EventType;
use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::create([
            'title' => [
                'en' => 'Conferences',
                'ru' => 'Конференции',
                'uz' => "Konferentsiyalar",
            ],
            'alias' => (string)\Str::of('Conferences')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Lectures',
                'ru' => 'Лекции',
                'uz' => "Ma'ruzalar",
            ],
            'alias' => (string)\Str::of('Lectures')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Internships',
                'ru' => 'Стажировки',
                'uz' => "Amaliyot",
            ],
            'alias' => (string)\Str::of('Internships')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Trainings',
                'ru' => 'Тренинги',
                'uz' => "Treninglar",
            ],
            'alias' => (string)\Str::of('Trainings')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Exhibitions',
                'ru' => 'Выставки',
                'uz' => "Ko'rgazmalar",
            ],
            'alias' => (string)\Str::of('Exhibitions')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Open doors days',
                'ru' => 'Дни открытых дверей',
                'uz' => "Ochiq eshiklar kuni",
            ],
            'alias' => (string)\Str::of('Open doors days')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Hackathons',
                'ru' => 'Хакатоны',
                'uz' => "Xakatonlar",
            ],
            'alias' => (string)\Str::of('Hackathons')->slug()
        ]);

        EventType::create([
            'title' => [
                'en' => 'Festivals',
                'ru' => 'Фестивали',
                'uz' => "Bayramlar",
            ],
            'alias' => (string)\Str::of('Festivals')->slug()
        ]);
    }
}
