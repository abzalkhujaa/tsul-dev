<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => 'abzalkhujaa',
            'password' => \Hash::make("qwerty123$"),
            'email' => 'abzalkhujaa@gmail.com',
        ]);

        $user->setTranslation('first_name','en','Abzalkhuja');
        $user->setTranslation('last_name','en','Abrolkhujaev');
        $user->setTranslation('patronymic','en','Ahmadkhuja ugli');
        $user->save();
    }
}
