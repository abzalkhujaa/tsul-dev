<?php

namespace Database\Seeders;

use App\Models\EventTheme;
use Illuminate\Database\Seeder;

class EventThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventTheme::create([
            'title' => [
                'en' => 'The science',
                'ru' => 'Наука',
                'uz' => "Ilm-fan",
            ],
            'alias' => (string)\Str::of('The science')->slug()
        ]);

        EventTheme::create([
            'title' => [
                'en' => 'Education',
                'ru' => 'Образование',
                'uz' => "Ta'lim",
            ],
            'alias' => (string)\Str::of('Education')->slug()
        ]);

        EventTheme::create([
            'title' => [
                'en' => 'Startups and business',
                'ru' => 'Стартапы и бизнес',
                'uz' => "Startaplar va biznes",
            ],
            'alias' => (string)\Str::of('Startups and business')->slug()
        ]);

        EventTheme::create([
            'title' => [
                'en' => 'Leisure',
                'ru' => 'Досуг',
                'uz' => "Bo'sh vaqt",
            ],
            'alias' => (string)\Str::of('Leisure')->slug()
        ]);
    }
}
