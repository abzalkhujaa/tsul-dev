<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $en = [
            'en' => 'English',
            'ru' => 'Английский',
            'uz' => 'Inglizcha',
        ];
        $model_en = new Language(['alias' => 'en','icon' => 'flag-icon flag-icon-gb']);
        $model_en->setTranslations('title',$en);
        $model_en->save();

        $ru = [
            'en' => 'Russian',
            'ru' => 'Русский',
            'uz' => 'Ruscha',
        ];
        $model_ru = new Language(['alias' => 'ru','icon' => 'flag-icon flag-icon-ru']);
        $model_ru->setTranslations('title',$ru);
        $model_ru->save();

        $uz = [
            'en' => 'Uzbek',
            'ru' => 'Узбекский',
            'uz' => 'O`zbekcha'
        ];
        $model_uz = new Language(['alias' => 'uz','icon' => 'flag-icon flag-icon-uz']);
        $model_uz->setTranslations('title',$uz);
        $model_uz->save();
    }
}
