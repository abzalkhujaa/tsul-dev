<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            UserRoleSeeder::class,
            LanguageSeeder::class,
            CategorySeeder::class,
            PageTypeSeeder::class,
            TagSeed::class,
            EventThemeSeeder::class,
            EventTypeSeeder::class,
            PostAndTagSeeder::class
//            MenuSeeder::class,
        ]);
    }
}
